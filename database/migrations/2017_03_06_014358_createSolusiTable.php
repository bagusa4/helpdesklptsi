<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolusiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TiketSolution', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tiket_id')->unsigned();
            $table->string('analisis')->nullable();
            $table->string('solusi')->nullable();

            $table->foreign('tiket_id')->references('id')->on('Tiket');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TiketSolution');
    }
}
