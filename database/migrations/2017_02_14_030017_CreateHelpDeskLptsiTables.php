<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHelpDeskLptsiTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Create Identitas Users Table
        if (!Schema::hasTable('IdentitasUsers') && Schema::hasTable('users')) {
            Schema::create("IdentitasUsers", function (Blueprint $table) {
                $table->increments('id');
                $table->string('nama_lengkap')->nullable();
                $table->date('tanggal_lahir')->nullable();
                $table->string('tempat_lahir')->nullable();
                $table->string('alamat')->nullable();
                $table->string('status')->nullable();
                $table->string('jabatan')->nullable();
                $table->string('photo_url')->nullable();
                $table->integer('auth_user_id')->unsigned();

                $table->foreign('auth_user_id')->references('id')->on('users');
            });
        }


        // Create Teknisi Table
        /*if (!Schema::hasTable('Teknisi') && Schema::hasTable('users')) {
            Schema::create('Teknisi', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('user_id')->unsigned();
                $table->string('jenis_teknisi')->nullable();

                $table->foreign('user_id')->references('id')->on('users');
            });
        }*/


        // Tiket Relation with Teknisi
        if (!Schema::hasTable('Tiket') && Schema::hasTable('users') && Schema::hasTable('UnitKerja')) {
            Schema::create('Tiket', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('user_id')->unsigned();
                $table->integer('unit_kerja_id')->unsigned();
                $table->string('nama_pelapor')->nullable();
                $table->string('nama_tiket');
                $table->string('prioritas_tiket')->nullable();
                $table->string('status_tiket')->nullable();
                $table->string('deskripsi_tiket')->nullable();
                $table->integer('vote_tiket')->nullable();
                $table->boolean('faq')->default(false);
                $table->timestamps(); /** before => $table->date('tanggal_tiket'); but i think isnt flexible */

                $table->foreign('user_id')->references('id')->on('users');
                $table->foreign('unit_kerja_id')->references('id')->on('UnitKerja');
            });
        }

        if (!Schema::hasTable('UsersTeknisi_Tiket')) {
            if (Schema::hasTable('users') && Schema::hasTable('Tiket')) {
                Schema::create('UsersTeknisi_Tiket', function (Blueprint $table) {
                    $table->integer('user_id')->unsigned();
                    $table->integer('tiket_id')->unsigned();

                    $table->foreign('user_id')->references('id')->on('users');
                    $table->foreign('tiket_id')->references('id')->on('Tiket');

                    $table->primary(array('user_id', 'tiket_id'));
                });
            }
        }


        // Tiket Info relation with Teknisi and Tiket
        if (!Schema::hasTable('TiketInfo')) {
            if (Schema::hasTable('users') && Schema::hasTable('Tiket')) {
                Schema::create('TiketInfo', function (Blueprint $table) {
                    $table->increments('id');
                    $table->integer('user_id')->unsigned();
                    $table->integer('tiket_id')->unsigned();
                    $table->string('status_info')->nullable();
                    $table->string('deskripsi_info')->nullable();
                    $table->timestamps();

                    $table->foreign('user_id')->references('id')->on('users');
                    $table->foreign('tiket_id')->references('id')->on('Tiket');
                });
            }
        }

        /*if (!Schema::hasTable('TiketInfo_Tiket')) {
            if (Schema::hasTable('TiketInfo') && Schema::hasTable('Tiket')) {
                Schema::create('TiketInfo_Tiket', function (Blueprint $table) {
                    $table->integer('tiket_id')->unsigned();
                    $table->integer('tiket_info_id')->unsigned();

                    $table->foreign('tiket_id')->references('id')->on('Tiket');
                    $table->foreign('tiket_info_id')->references('id')->on('TiketInfo');

                    $table->primary('tiket_id', 'tiket_info_id');
                });
            }
        }*/


        // Tiket Categori relation with Tiket and Tiket Kategori
        if (!Schema::hasTable('KategoriTiket')) {
            Schema::create('KategoriTiket', function (Blueprint $table) {
                $table->increments('id');
                $table->string('nama_kategori');
                $table->string('deskripsi_kategori')->nullable();
            });
        }

        if (!Schema::hasTable('KategoriTiket_Tiket')) {
            if (Schema::hasTable('KategoriTiket') && Schema::hasTable('Tiket')) {
                Schema::create('KategoriTiket_Tiket', function (Blueprint $table) {
                    $table->integer('tiket_id')->unsigned();
                    $table->integer('kategori_tiket_id')->unsigned();

                    $table->foreign('tiket_id')->references('id')->on('Tiket');
                    $table->foreign('kategori_tiket_id')->references('id')->on('KategoriTiket');

                    $table->primary(array('tiket_id', 'kategori_tiket_id'));
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Schema::dropIfExists('TiketInfo_Tiket');
        Schema::dropIfExists('KategoriTiket_Tiket');
        Schema::dropIfExists('UsersTeknisi_Tiket');

        Schema::dropIfExists('TiketInfo');
        Schema::dropIfExists('KategoriTiket');
        Schema::dropIfExists('Tiket');

        //Schema::dropIfExists('Teknisi');
        Schema::dropIfExists('IdentitasUsers');
    }
}
