<?php

use Illuminate\Database\Seeder;

class InsertTiketKategori extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('KategoriTIket')->insert([
            'nama_kategori' => 'Jaringan Wifi',
            'deskripsi_kategori' => 'Problem tentang koneksi WIFI'
        ]);

        DB::table('KategoriTIket')->insert([
            'nama_kategori' => 'Jaringan Lan',
            'deskripsi_kategori' => 'Problem tentang koneksi LAN'
        ]);

        DB::table('KategoriTIket')->insert([
            'nama_kategori' => 'Internet',
            'deskripsi_kategori' => 'Problem tentang koneksi Internet'
        ]);

        DB::table('KategoriTIket')->insert([
            'nama_kategori' => 'DLL',
            'deskripsi_kategori' => 'Kategori Lain'
        ]);
    }
}
