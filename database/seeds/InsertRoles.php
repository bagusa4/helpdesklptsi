<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class InsertRoles extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
         * Roles Rules
         * 1 => Admin
         * 2 => Operator
         * 3 => Teknisi
         * 4 => User
         */

        DB::table('roles')->insert([
            'name' => "Admin",
            'display_name' => "Admin",
            'description' => "",
            'created_at' => \Carbon\Carbon::now()
        ]);

        DB::table('roles')->insert([
            'name' => "Operator",
            'display_name' => "Operator",
            'description' => "",
            'created_at' => \Carbon\Carbon::now()
        ]);

        DB::table('roles')->insert([
            'name' => "Teknisi",
            'display_name' => "Teknisi",
            'description' => "",
            'created_at' => \Carbon\Carbon::now()
        ]);

        DB::table('roles')->insert([
            'name' => "User",
            'display_name' => "User",
            'description' => "",
            'created_at' => \Carbon\Carbon::now()
        ]);
    }
}
