<?php

use Illuminate\Database\Seeder;

class InsertDefaultUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\User::class)->create([
            'name' => 'Admin',
            'password' => bcrypt('freelaravel'),
            'active' => true
        ])->each(function ($user) {
            $user->roles()->attach(1);

            factory(App\Models\IdentitasUsers::class)->create([
                'auth_user_id' => $user->id,
                'nama_lengkap' => 'Default Admin LPTSI'
            ]);
        });

        factory(App\Models\User::class)->create([
            'name' => 'Operator',
            'password' => bcrypt('freelaravel'),
            'active' => true
        ])->each(function ($user) {
            $user->roles()->attach(2);

            factory(App\Models\IdentitasUsers::class)->create([
                'auth_user_id' => $user->id,
                'nama_lengkap' => 'Default Operator LPTSI'
            ]);
        });

        factory(App\Models\User::class)->create([
            'name' => 'Teknisi',
            'password' => bcrypt('freelaravel'),
            'active' => true
        ])->each(function ($user) {
            $user->roles()->attach(3);

            factory(App\Models\IdentitasUsers::class)->create([
                'auth_user_id' => $user->id,
                'nama_lengkap' => 'Default Teknisi LPTSI'
            ]);
        });

        factory(App\Models\User::class)->create([
            'name' => 'User',
            'password' => bcrypt('freelaravel'),
            'active' => true
        ])->each(function ($user) {
            $user->roles()->attach(4);

            factory(App\Models\IdentitasUsers::class)->create([
                'auth_user_id' => $user->id,
                'nama_lengkap' => 'Default User LPTSI'
            ]);
        });
    }
}
