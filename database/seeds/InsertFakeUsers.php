<?php

use Illuminate\Database\Seeder;

class InsertFakeUsers extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        factory(App\Models\User::class, 50)->create()->each(function ($user) use ($faker) {
            $user->roles()->attach($faker->numberBetween(1, 4));

            factory(App\Models\IdentitasUsers::class)->create([
                'auth_user_id' => $user->id,
                'nama_lengkap' => $user->name
            ]);
        });

        /*foreach (range(1, 40) as $index) {
            DB::table('users')->insert([
                'name' => $faker->name,
                'email' => $faker->email,
                'password' => bcrypt($faker->password),
                'created_at' => $faker->dateTime,
                'active' => $faker->numberBetween(0, 1)
            ]);
        }*/
    }
}
