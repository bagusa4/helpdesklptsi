<?php

use Illuminate\Database\Seeder;

class InsertUnitKerja extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /** Fakultas Pertanian */
        $fp = [
            'S1  Agroteknologi',
            'S1  Agribisnis',
            'S1  Teknik Pertanian',
            'S1  Ilmu dan Teknologi Pangan',
            'D3  Agribisnis',
            'D3  Perencanaan Sumber Daya Lahan'
        ];
        foreach ($fp as $p) {
            DB::table('UnitKerja')->insert([
                'nama' => $p,
                'deskripsi' => 'Fakultas Pertanian'
            ]);
        }

        /** Fakultas Biologi */
        $fb = [
            'S1 Biologi',
            'D3 Pengelolaan Sumberdaya Perikanan'
        ];
        foreach ($fb as $b) {
            DB::table('UnitKerja')->insert([
                'nama' => $b,
                'deskripsi' => 'Fakultas Biologi'
            ]);
        }

        /** Fakultas Ekonomi dan Bisnis */
        $feb = [
            'S1 Manajemen',
            'S1 Ilmu Ekonomi & Studi Pembangunan',
            'S1 Akuntansi',
            'D3 Administrasi Keuangan',
            'D3 Kesekretariatan',
            'D3 Bisnis Internasional',
            'D3 Akuntansi',
            'S1 Pendidikan Ekonomi',
            'Program Profesi Akuntan'
        ];
        foreach ($feb as $eb) {
            DB::table('UnitKerja')->insert([
                'nama' => $eb,
                'deskripsi' => 'Fakultas Ekonomi dan Bisnis'
            ]);
        }

        /** Fakultas Peternakan */
        $fternak = [
            'S1 Peternakan',
            'D3 Produksi Ternak'
        ];
        foreach ($fternak as $ft) {
            DB::table('UnitKerja')->insert([
                'nama' => $ft,
                'deskripsi' => 'Fakultas Pertenakan'
            ]);
        }

        /** Fakultas Hukum */
        $fh = [
            'S1 – Ilmu Hukum'
        ];
        foreach ($fh as $h) {
            DB::table('UnitKerja')->insert([
                'nama' => $h,
                'deskripsi' => 'Fakultas Hukum'
            ]);
        }

        /** Fakultas Ilmu Sosial dan Politik */
        $fisp = [
            'S1 Sosiologi',
            'S1 Ilmu Administrasi Negara',
            'S1 Ilmu Komunikasi',
            'S1 Ilmu Politik',
            'S1 Ilmu Hubungan Internasional (dalam proses akreditasi)',
            'S1 Sastra Inggris',
            'S1 Sastra Indonesia',
            'S1 Sastra Jepang (dalam proses akreditasi)',
            'D3 Bahasa Inggris',
            'D3 Bahasa Mandarin (dalam proses akreditasi)',
            'S1 Pendidikan Bahasa dan Sastra Indonesia'
        ];
        foreach ($fisp as $isp) {
            DB::table('UnitKerja')->insert([
                'nama' => $isp,
                'deskripsi' => 'Fakultas Ilmu Sosial dan Politik'
            ]);
        }

        /** Fakultas Kedokteran dan Ilmu-ilmu Kesehatan */
        $fkiik = [
            'S1 Pendidikan Dokter',
            'S1 Pendidikan Dokter Gigi',
            'S1 Kesehatan Masyarakat',
            'S1 Farmasi',
            'S1 Ilmu Keperawatan',
            'S1 Ilmu Gizi (dalam proses akreditasi)',
            'S1 Pendidikan Jasmani, Kesehatan, dan Rekreasi',
            'Program Profesi Dokter',
            'Program Profesi Ners',
            'Program Profesi Dokter Gigi'
        ];
        foreach ($fkiik as $kiik) {
            DB::table('UnitKerja')->insert([
                'nama' => $kiik,
                'deskripsi' => 'Fakultas Kedokteran dan Ilmu-ilmu Kesehatan'
            ]);
        }

        /** Fakultas Sains dan Teknik */
        $fst = [
            'S1 Matematika',
            'S1 Fisika',
            'S1 Kimia',
            'S1 Manajemen Sumberdaya Perairan',
            'S1 Budidaya Perairan',
            'S1 Ilmu Kelautan',
            'S1 Teknik Elektro',
            'S1 Teknik Sipil',
            'S1 Teknik Geologi',
            'S1 Teknik Informatika',
            'D3  Teknik Industri (dalam proses akreditasi)'
        ];
        foreach ($fst as $st) {
            DB::table('UnitKerja')->insert([
                'nama' => $st,
                'deskripsi' => 'Fakultas Sains dan Teknik'
            ]);
        }

        DB::table('UnitKerja')->insert([
            'nama' => 'Lembaga LPTSI Unsoed',
            'deskripsi' => 'Lembaga Unsoed'
        ]);
    }
}
