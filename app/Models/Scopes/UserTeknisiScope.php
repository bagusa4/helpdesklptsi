<?php
/**
 * Created by PhpStorm.
 * User: Bagusa4 - Smecon
 * Date: 3/18/2017
 * Time: 3:37 PM
 */

namespace App\Models\Scopes;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class UserTeknisiScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $builder
     * @param  \Illuminate\Database\Eloquent\Model $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        $builder->whereHas('roles', function ($q) {
            $q->where('name', 'Teknisi');
        });
    }

}