<?php

namespace App\Models;

use App\Models\Scopes\CreatedAtScope;
use App\Models\Traits\LocalQueryScopeTrait;
use Illuminate\Database\Eloquent\Model;

class Tiket extends Model
{
    use LocalQueryScopeTrait;

    /**
     * Do Something function when some event fire on parent
     */
    protected static function boot () {
        parent::boot();

        static::addGlobalScope(new CreatedAtScope());

        static::deleting(function ($tiket)
        {
            $tiket->solution()->delete();
            $tiket->tiketInfo()->delete();
            $tiket->kategori()->detach();
            $tiket->teknisi()->detach();
        });
        static::deleted(function ($tiket)
        {
            //
        });
    }

    /**
     * The attribute that for select spesific Table
     *
     * @var string
     */
    protected $table = 'Tiket';

    /**
     * The attribute that for disable auto timestamp when insert new data
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The fillable rule on the table
     *
     * @var array
     */
    /*protected $fillable = [
        'user_id', 'nama_tiket', 'prioritas_tiket', 'status_tiket', 'deskripsi_tiket', 'tanggal_tiket', 'vote_tiket'
    ];*/

    /**
     * The guarded filter the fillable they can't be fill
     *
     * @var array
     */
    protected $guarded = [

    ];

    /**
     * Override this method to assign specific columns for references result
     *
     * @return array
     */
    protected function searchColumns()
    {
        return ["nama_pelapor", "nama_tiket", "deskripsi_tiket", 'status_tiket', 'prioritas_tiket'];
    }

    /**
     * belongsTo user
     * 'App\Models\User'
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * belongsTo UnitKerja
     * 'App\Models\UnitKerja'
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function unitKerja()
    {
        return $this->belongsTo(UnitKerja::class, 'unit_kerja_id', 'id');
    }

    /**
     * belongsToMany Teknisi
     * 'App\Models\Teknisi'
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function teknisi()
    {
        return $this->belongsToMany(Teknisi::class, 'UsersTeknisi_Tiket', 'tiket_id', 'user_id');
    }

    /**
     * belongsToMany Kategori
     * 'App\Models\KategoriTiket'
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function kategori()
    {
        return $this->belongsToMany(KategoriTiket::class, 'KategoriTiket_Tiket');
    }

    /**
     * hasMany TiketInfo
     * 'App\Models\TiketInfo'
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tiketInfo()
    {
        return $this->hasMany(TiketInfo::class, 'tiket_id', 'id');
    }

    /**
     * hasOne Solution
     * 'App\Models\TiketSolution'
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function solution()
    {
        return $this->hasOne(TiketSolution::class, 'tiket_id', 'id');
    }

    /**
     * find tiket with faq true
     *
     * @param $query
     * @return mixed
     */
    public function scopeFaq($query)
    {
        return $query->where('faq', true);
    }

    /**
     * get all tiket data with specific user role
     *
     * @param $query
     * @param $id
     * @return mixed
     */
    public function scopeWithUserRole($query, $id)
    {
        return $query->withoutGlobalScope(CreatedAtScope::class)->whereHas('user', function ($query) use ($id) {
            $query->withoutGlobalScope(CreatedAtScope::class)->whereHas('roles', function ($query) use ($id) {
                if (!is_null($id)) {
                    $query->withoutGlobalScope(CreatedAtScope::class)->where('id', $id);
                }
            });
        });
    }

    /**
     * get all tiket with specific unit kerja
     *
     * @param $query
     * @param $id
     * @return mixed
     */
    public function scopeWithUnitKerja($query, $id)
    {
        return $query->whereHas('unitKerja', function ($query) use ($id) {
            if (!is_null($id)) {
                $query->where('id', $id);
            }
        });
    }

    /**
     * Get tiket with spesific user
     *
     * @param $id
     * @return mixed
     */
    public static function withUser($id)
    {
        return self::whereHas('user', function ($q) use ($id) {
           $q->withoutGlobalScope(CreatedAtScope::class)->where('id', $id);
        });
    }

    /**
     * Get tiket with spesific teknisi
     *
     * @param $id
     * @return mixed
     */
    public static function withTeknisi($id)
    {
        return self::whereHas('teknisi', function ($q) use ($id) {
            $q->withoutGlobalScope(CreatedAtScope::class)->where('id', $id);
        });
    }
}
