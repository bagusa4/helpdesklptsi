<?php

namespace App\Models;

use App\Models\Scopes\UserTeknisiScope;
use App\Models\User;

class Teknisi extends User
{
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new UserTeknisiScope());
    }

    /**
     * The attribute that for select spesific Table
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * belongsToMany Tikets work
     * 'App\Models\Tiket'
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tiket()
    {
        return $this->belongsToMany(Tiket::class, 'UsersTeknisi_Tiket', 'user_id', 'tiket_id');
    }
}
