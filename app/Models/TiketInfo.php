<?php

namespace App\Models;

use App\Models\Scopes\CreatedAtScope;
use Illuminate\Database\Eloquent\Model;

class TiketInfo extends Model
{
    /**
     * The attribute that for select spesific Table
     *
     * @var string
     */
    protected $table = 'TiketInfo';

    /**
     * The guarded filter the fillable they can't be fill
     *
     * @var array
     */
    protected $guarded = [

    ];

    /**
     * belongsTo Tiket
     * 'App\Models\Tiket'
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tiket()
    {
        return $this->belongsTo(Tiket::class, 'tiket_id', 'id');
    }

    /**
     * belongsTo Teknisi
     * 'App\Models\User'
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * Get info with spesific tiket
     *
     * @param $id
     * @return mixed
     */
    public static function withTiket($id)
    {
        return self::whereHas('tiket', function ($q) use ($id) {
           $q->withoutGlobalScope(CreatedAtScope::class)->where('id', $id);
        });
    }
}
