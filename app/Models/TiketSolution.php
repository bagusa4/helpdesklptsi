<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TiketSolution extends Model
{
    /**
     * The attribute that for select spesific Table
     *
     * @var string
     */
    protected $table = 'TiketSolution';

    /**
     * The attribute that for disable auto timestamp when insert new data
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The guarded filter the fillable they can't be fill
     *
     * @var array
     */
    protected $guarded = [

    ];

    /**
     * belongsTo Tiket
     * 'App\Models\Tiket'
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tiket()
    {
        return $this->belongsTo(Tiket::class, 'tiket_id', 'id');
    }
}
