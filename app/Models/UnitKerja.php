<?php

namespace App\Models;

use App\Models\Traits\LocalQueryScopeTrait;
use Illuminate\Database\Eloquent\Model;

class UnitKerja extends Model
{
    use LocalQueryScopeTrait;

    /**
     * The attribute that for select spesific Table
     *
     * @var string
     */
    protected $table = 'UnitKerja';

    /**
     * The attribute that for disable auto timestamp when insert new data
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The guarded filter the fillable they can't be fill
     *
     * @var array
     */
    protected $guarded = [

    ];

    /**
     * Override this method to assign spesific column for ordering
     *
     * @return string
     */
    protected function orderingColumn()
    {
        return 'nama';
    }

    /**
     * Override this method to assign specific columns for references result
     *
     * @return array
     */
    protected function searchColumns()
    {
        return ["nama", "deskripsi"];
    }

    /**
     * hasMany to User
     * 'App\Models\User'
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function user()
    {
        return $this->hasMany(User::class, 'unit_kerja_id', 'id');
    }

    /**
     * hasMany tiket Through users
     * 'App\Models\Tiket'
     * 'App\Models\User'
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function tiketThroughUser()
    {
        return $this->hasManyThrough(Tiket::class, User::class, 'unit_kerja_id', 'user_id', 'id');
    }

    /**
     * hasMany to Tiket
     * 'App\Models\Tiket'
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tiket()
    {
        return $this->hasMany(Tiket::class, 'unit_kerja_id', 'id');
    }
}
