<?php

namespace App\Models\Traits;

use App\Models\Scopes\CreatedAtScope;
use Illuminate\Database\Eloquent\Model;

trait LocalQueryScopeTrait
{
    /**
     * Override this method to custom created_at column or change const CREATED_AT property
     * default created_at
     * return must be string of column name
     *
     * @return string
     */
    protected function dateColumn()
    {
        return $this->CREATED_AT ? $this->CREATED_AT : "created_at";
    }

    /**
     * Override this method to custom orderBy column
     * default created_at
     * return must be string of column name
     *
     * @return string
     */
    protected function orderingColumn()
    {
        return $this->dateColumn();
    }

    /**
     * Define specific columns as references
     *
     * @return array
     */
    protected function searchColumns()
    {
        return null;
    }

    /**
     * filter data base on date
     *
     * @param $query
     * @param $date
     * @return mixed
     */
    public function scopeOnDate($query, $date)
    {
        if (!is_null($date)) {
            return $query->withoutGlobalScope(CreatedAtScope::class)->whereDate($this->dateColumn(), $date);
        }

        return $query;
    }

    /**
     * filter data base on month
     *
     * @param $query
     * @param $month
     * @return mixed
     */
    public function scopeOnMonth($query, $month)
    {
        if (!is_null($month)) {
            return $query->withoutGlobalScope(CreatedAtScope::class)->whereMonth($this->dateColumn(), $month);
        }

        return $query;
    }

    /**
     * Local scope method to order result by ASC or DESC, through 'created_at' column
     *
     * @param $query
     * @param $order
     * @return mixed
     */
    public function scopeOrderingBy($query, $order)
    {
        if (!is_null($order)) {
            return $query->withoutGlobalScope(CreatedAtScope::class)->orderBy($this->orderingColumn(), $order);
        }

        return $query;
    }

    /**
     * Local scope method to find data with spesific ID
     *
     * @param $query
     * @param $id
     * @return mixed
     */
    public function scopeFindById($query, $id)
    {
        if (!is_null($id)) {
            return $query->where('id', $id);
        }

        return $query;
    }

    /**
     * As describe in https://laravel.com/docs/5.4/eloquent#query-scopes
     * scope function on model class will have instance of \Illuminate\Database\Eloquent\Builder
     * on $query paremeter (the first parameter), and second parameters is options.
     *
     * Overload searchColumns() method to assign specific columns for references result
     *
     * @param $query
     * @param $keywords
     * @return mixed
     * @throws \Exception
     */
    public function scopeSearch($query, $keywords)
    {
        if (is_null($this->searchColumns()) == null && !is_array($this->searchColumns())) {
            throw new \Exception("Oops sorry, please override searchColumns() method. :)");
        }

        if (!is_null($keywords)) {
            return $this->searchMethod($query, $this->searchColumns(), $keywords);
        }

        return $query;
    }

    /**
     * Search Method,
     * $query parameter will have instance of \Illuminate\Database\Eloquent\Builder
     * $columns parameter will have specific columns to evaluate result
     * $keywords parameter that use as reference based on $columns
     *
     * @param $query
     * @param array $columns
     * @param $keywords
     * @return mixed
     */
    protected function searchMethod($query, $columns = [], $keywords)
    {
        $query->where(function ($query) use ($columns, $keywords) {
            $i = 0;
            foreach ($columns as $column) {
                if ($i == 0) {
                    /* First Iterate */
                    call_user_func_array(array($query, "where"), array($column, "LIKE", "%$keywords%"));
                    //$query->where($column, "LIKE", "%$keywords%");
                }

                /* Second ... Iterates */
                call_user_func_array(array($query, "orWhere"), array($column, "LIKE", "%$keywords%"));
                //$query->orWhere($column, "LIKE", "%$keywords%");

                ++$i;
            }
        });

        return $query;
    }
}
