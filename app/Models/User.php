<?php

namespace App\Models;

use App\Models\Scopes\CreatedAtScope;
use App\Models\Traits\LocalQueryScopeTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes, LocalQueryScopeTrait, EntrustUserTrait {
        SoftDeletes::restore as SoftDeletesRestore;
        EntrustUserTrait::restore as EntrustRestore;
    }

    /**
     * Do Something function when some event fire on parent
     */
    protected static function boot () {
        parent::boot();

        static::addGlobalScope(new CreatedAtScope());

        static::deleting(function ($user)
        {
            if ($user->isForceDeleting()) {
                $user->identitas()->forceDelete();

                if ($user->tiket) {
                    $user->emptyTiket();
                }
            } else {
                $user->identitas()->delete();
            }
        });

        static::deleted(function ($user)
        {

        });

        static::restored(function ($user)
        {
            $user->identitas()->withTrashed()->restore();
        });
    }

    /**
     * Temporary fix bug at entrust trait restore method,
     * with soft deletes method on SoftDeletes trait
     *
     * https://github.com/Zizaco/entrust/issues/379
     * https://github.com/Zizaco/entrust/issues/306
     *
     * @param bool $entrust
     * @return bool|null
     */
    public function restore($entrust = false)
    {
        $result = $this->SoftDeletesRestore();
        if ($entrust) {
            $this->EntrustRestore();
        }
        return $result;
    }

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'active', 'unit_kerja_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Override this method to assign specific columns for references result
     *
     * @return array
     */
    protected function searchColumns()
    {
        return ["name", "email"];
    }

    /**
     * belongsTo UnitKerja
     * 'App\Models\UnitKerja'
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function unitKerja()
    {
        return $this->belongsTo(UnitKerja::class, 'unit_kerja_id', 'id');
    }

    /**
     * hasOne IdentitasUsers
     * 'App\Models\IdentitasUsers'
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function identitas()
    {
        return $this->hasOne(IdentitasUsers::class, 'auth_user_id', 'id');
    }

    /**
     * hasOne Teknisi
     * 'App\Models\Teknisi'
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function teknisi()
    {
        return $this->hasOne(Teknisi::class, 'user_id', 'id');
    }

    /**
     * hasMany Tiket
     * 'App\Models\Tiket'
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tiket()
    {
        return $this->hasMany(Tiket::class, 'user_id', 'id');
    }

    /**
     * hasMany TiketInfo
     * 'App\Models\TiketInfo'
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tiketInfo()
    {
        return $this->hasMany(TiketInfo::class, 'user_id', 'id');
    }

    /**
     * Empty all tiket when user deleted
     */
    public function emptyTiket()
    {
        $tikets = $this->tiket;

        foreach ($tikets as $tiket) {
            $tiket->user()->dissociate();
            $tiket->save();
        }

        $this->load('tiket');
    }

    /**
     * Get all users with spesific roles
     *
     * @param $name
     * @return mixed
     */
    public static function withRole($name = null)
    {
        return self::whereHas('roles', function ($q) use ($name) {
            if ($name !== null) {
                $q->where('name', $name);
            }
        });
    }

    /**
     * Get all users where active column is false
     *
     * @return mixed
     */
    public static function notActive()
    {
        return self::where('active', false);
    }
}
