<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class IdentitasUsers extends Model
{
    use SoftDeletes;

    /**
     * Do Something function when some event fire on parent
     */
    protected static function boot () {
        parent::boot();

        static::deleted(function ($identitasUsers)
        {
            if ($identitasUsers->user) {
                //$identitasUsers->user()->delete();
            }
        });
    }

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attribute that for select spesific Table
     *
     * @var string
     */
    protected $table = 'IdentitasUsers';

    /**
     * All of the relationships to be touched.
     *
     * @var array
     */
    protected $touches = ['user'];

    /**
     * The attribute that for disable auto timestamp when insert new data
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Guarding the mass asigment
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Blongs to AuthUsers
     * 'App\Models\User'
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'auth_user_id', 'id');
    }
}
