<?php

namespace App\Console\Commands\HelpDeskLptsi;

use Illuminate\Console\Command;

class Build extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'helpdesklptsi:build 
                            {--M|database : Buat Relasi Database}
                            {--D|default : Buat Default Data}
                            {--R|roles : Buat Roles}
                            {--P|permissions : Buat Permissions}  
                            {--F|faker : Buat Data Palsu untuk percobaan}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make Build [ Database | Default Data | Roles | Permissions | Faker Data ] for Help Desk LPTSI';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $ask = $this->ask("Apakah ingin melakukan build ???");

        if ($ask) {
            if ($this->option('database')) {
                $this->migrate();
            }

            if ($this->options('default')) {
                $this->defaultData();
            }

            if ($this->option('roles')) {
                $this->roles();
            }

            if ($this->option('permissions')) {
                $this->permissions();
            }

            if ($this->option('faker')) {
                $this->faker();
            }
        } else {
            $this->info("Build dibatalkan !");
        }
    }

    private function migrate()
    {
        $this->call('migrate');
    }

    private function roles()
    {
        $this->call('db:seed', [
            "--class" => "InsertRoles"
        ]);
    }

    private function permissions()
    {

    }

    private function faker()
    {
        $this->call('db:seed', [
            "--class" => "InsertFakeUsers"
        ]);

        $this->call('db:seed', [
            "--class" => "InsertFakeIdentitasUsers"
        ]);
    }

    private function defaultData()
    {
        $this->roles();

        $this->call('db:seed', [
            "--class" => "InsertUnitKerja"
        ]);

        $this->call('db:seed', [
            "--class" => 'InsertDefaultUser'
        ]);

        $this->call('db:seed', [
            "--class" => 'InsertTiketKategori'
        ]);
    }
}
