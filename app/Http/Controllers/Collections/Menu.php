<?php
/**
 * Created by PhpStorm.
 * User: Bagusa4 - Smecon
 * Date: 3/31/2017
 * Time: 4:23 PM
 */

namespace App\Http\Controllers\Collections;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\URL;

class Menu
{
    /* -------------------------------------------------
    |
    |   Pengaturan Menu,
    |   setiap kali ada instance yang di buat, maka akan menjalankan method setters 'setMenu',
    |   lalu dengan static method getters 'getMenu', akan mengembalikan hasil koleksi menu berupa object/array.
    |   kelas ini akan di pakai pada controller Home, dan ViewComposers -> MenuViewComposer, dimana akan menetapkan data
    |   dari kelas ini ke semua view, karena digunakan untuk menampilkan menu pada navbar.
    |
    ---------------------------------------------------- */

    private static $instance;
    private $menu;

    public function __construct()
    {
        $this->menu = collect();

        !Auth::guest() ? $this->setMenu() : null;
    }

    /**
     * Singleton
     *
     * @return Menu
     */
    public static function getInstance()
    {
        if (! is_object(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @return mixed
     */
    public static function getMenu()
    {
        return self::getInstance()->menu;
    }

    /**
     * @do set menu
     */
    private function setMenu()
    {
        if(Auth::user()->hasRole(['Admin','Operator'])) {
            $this->manageMenu();
        }

        if(Auth::user()->hasRole(['Teknisi'])) {

        }

        if(Auth::user()->hasRole(['User'])) {

        }

        $this->tiketMenu();
        $this->profileMenu();
    }

    private function manageMenu()
    {
        $manage = collect();

        if(Auth::user()->hasRole(['Admin', 'Operator'])) {
            if(Auth::user()->hasRole(['Admin'])) {
                $usersMenu = collect([
                    'name' => Lang::get('alias.manage.user.all.all'),
                    'description' => Lang::get('alias.manage.user.all.detail'),
                    'url' => URL::route('manage.allusers.index')
                ]);
                $manage->push($usersMenu);
            }

            $userMenu = collect([
                'name' => Lang::get('alias.manage.user.user'),
                'description' => Lang::get('alias.manage.user.detail'),
                'url' => URL::route('manage.user.index')
            ]);
            $manage->push($userMenu);

            $operatorMenu = collect([
                'name' => Lang::get('alias.manage.user.operator.operator'),
                'description' => Lang::get('alias.manage.user.operator.detail'),
                'url' => URL::route('manage.operator.index')
            ]);
            $manage->push($operatorMenu);

            $teknisiMenu = collect([
                'name' => Lang::get('alias.manage.user.teknisi.teknisi'),
                'description' => Lang::get('alias.manage.user.teknisi.detail'),
                'url' => URL::route('manage.teknisi.index')
            ]);
            $manage->push($teknisiMenu);

            if(Auth::user()->hasRole(['Admin'])) {
                $adminMenu = collect([
                    'name' => Lang::get('alias.manage.user.admin.admin'),
                    'description' => Lang::get('alias.manage.user.admin.detail'),
                    'url' => URL::route('manage.admin.index')
                ]);
                $manage->push($adminMenu);
            }

            $confirmationMenu = collect([
                'name' => Lang::get('alias.manage.confirmation.user.user'),
                'description' => Lang::get('alias.manage.confirmation.user.detail'),
                'url' => URL::route('manage.confirmation.users.index')
            ]);
            $manage->push($confirmationMenu);

            $trashedUsersMenu = collect([
                'name' => Lang::get('alias.manage.trashed.user.user'),
                'description' => Lang::get('alias.manage.trashed.user.detail'),
                'url' => URL::route('manage.trashed.users.index')
            ]);
            $manage->push($trashedUsersMenu);

            $unitKerjaMenu = collect([
                'name' => Lang::get('alias.manage.unit_kerja.unit_kerja'),
                'description' => Lang::get('alias.manage.unit_kerja.detail'),
                'url' => URL::route('manage.unitkerja.index')
            ]);
            $manage->push($unitKerjaMenu);

            $registerMenu = collect([
                'name' => Lang::get('alias.manage.register.register'),
                'description' => Lang::get('alias.manage.register.detail'),
                'url' => URL::route('manage.user.register')
            ]);
            $manage->push($registerMenu);
        }

        $this->menu->put('manage', $manage);
    }

    private function tiketMenu()
    {
        $tiket = collect();

        if (Auth::user()->hasRole(['Admin', 'Operator'])) {
            $manageTiket = collect([
                'name' => Lang::get('alias.tiket.manage.manage'),
                'description' => Lang::get('alias.tiket.manage.detail'),
                'url' => URL::route('manage.tiket.index')
            ]);
            $tiket->push($manageTiket);

            $manageKategoriTiket = collect([
                'name' => Lang::get('alias.tiket.kategori.kategori'),
                'description' => Lang::get('alias.tiket.kategori.detail'),
                'url' => URL::route('manage.tiket.kategori.index')
            ]);
            $tiket->push($manageKategoriTiket);

            $manageRekapTiket = collect([
                'name' => Lang::get('alias.tiket.rekap.rekap'),
                'description' => Lang::get('alias.tiket.rekap.detail'),
                'url' => URL::route('manage.tiket.rekap.index')
            ]);
            $tiket->push($manageRekapTiket);
        }

        if (Auth::user()->hasRole(['Teknisi'])) {
            $kerjaanKu = collect([
                'name' => Lang::get('alias.tiket.work.work'),
                'description' => Lang::get('alias.tiket.work.detail'),
                'url' => URL::route('teknisi.tiket.index')
            ]);
            $tiket->push($kerjaanKu);
        }

        $lihatSemua = collect([
            'name' => Lang::get('alias.tiket.faq.faq'),
            'description' => Lang::get('alias.tiket.faq.detail'),
            'url' => URL::route('tiket.index')
        ]);
        $tiket->push($lihatSemua);

        $tambahTiket = collect([
            'name' => Lang::get('alias.tiket.create.create'),
            'description' => Lang::get('alias.tiket.create.detail'),
            'url' => URL::route('tiket.ku.create')
        ]);
        $tiket->push($tambahTiket);

        $tiketKu = collect([
            'name' => Lang::get('alias.tiket.ku.ku'),
            'description' => Lang::get('alias.tiket.ku.detail'),
            'url' => URL::route('tiket.ku.index')
        ]);
        $tiket->push($tiketKu);

        $this->menu->put('tiket', $tiket);
    }

    private function profileMenu()
    {
        $profile = collect();

        $show = collect([
            'name' => Lang::get('alias.profile.view.view'),
            'description' => Lang::get('alias.profile.view.detail'),
            'url' => URL::route('user.profile.index')
        ]);
        $profile->push($show);

        $update = collect([
            'name' => Lang::get('alias.profile.update.update'),
            'description' => Lang::get('alias.profile.update.detail'),
            'url' => URL::route('user.profile.edit')
        ]);
        $profile->push($update);

        $changePassword = collect([
            'name' => Lang::get('alias.profile.password.password'),
            'description' => Lang::get('alias.profile.password.detail'),
            'url' => URL::route('user.profile.edit.password')
        ]);
        $profile->push($changePassword);

        $this->menu->put('profile', $profile);
    }
}