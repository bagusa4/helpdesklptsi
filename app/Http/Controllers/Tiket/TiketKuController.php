<?php

namespace App\Http\Controllers\Tiket;

use App\Http\Controllers\Traits\TiketControllerTrait;
use App\Models\Tiket;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class TiketKuController extends Controller
{
    use TiketControllerTrait;

    /**
     * Override this method to get spesific data
     *
     * @param Tiket $tiket
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    protected function getData(Tiket $tiket)
    {
        return $tiket->withUser(Auth::user()->id)->with(['user', 'unitKerja']);
    }

    /**
     * Override this method to get spesific find data
     *
     * @param Tiket $tiket
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    protected function getFind(Tiket $tiket)
    {
        return $tiket->withUser(Auth::user()->id)->with(['kategori', 'user', 'teknisi', 'tiketInfo.user', 'solution', 'unitKerja']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('tiket.ku.tiket');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tiket.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'nama_pelapor' => 'required|max:255',
            'nama_tiket' => 'required|min:6|max:255',
            'deskripsi_tiket' => 'required|min:20|max:255',
            'kategori_tiket' => 'required',
            'unit_kerja' => 'required|not_in:null'
        ])->validate();

        $tiket = Tiket::create([
            'user_id' => Auth::user()->id,
            'unit_kerja_id' => $request->input('unit_kerja'),
            'nama_pelapor' => $request->input('nama_pelapor'),
            'nama_tiket' => $request->input('nama_tiket'),
            'deskripsi_tiket' => $request->input('deskripsi_tiket'),
            'vote_tiket' => 1,
            'faq' => false
        ]);
        $tiket->kategori()->attach($request->input('kategori_tiket'));

        return response()->json($tiket)->header('Content-Type', 'application/json');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tiket  $tiket
     * @return \Illuminate\Http\Response
     */
    public function show(Tiket $tiket)
    {
        return view('tiket.ku.show')->with('data', $tiket);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tiket  $tiket
     * @return \Illuminate\Http\Response
     */
    public function edit(Tiket $tiket)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tiket  $tiket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tiket $tiket)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tiket  $tiket
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tiket $tiket)
    {
        //
    }
}
