<?php

namespace App\Http\Controllers\Tiket;

use App\Http\Controllers\Traits\TiketControllerTrait;
use App\Models\Tiket;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TiketController extends Controller
{
    use TiketControllerTrait;

    /**
     * Override this method to get spesific data
     *
     * @param Tiket $tiket
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    protected function getData(Tiket $tiket)
    {
        return $tiket->with(['user', 'unitKerja'])->faq();
    }

    /**
     * Override this method to get spesific find data
     *
     * @param Tiket $tiket
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    protected function getFind(Tiket $tiket)
    {
        return $tiket->with(['kategori', 'user', 'teknisi', 'tiketInfo.user', 'solution', 'unitKerja'])->faq();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('tiket.tiket');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tiket  $tiket
     * @return \Illuminate\Http\Response
     */
    public function show(Tiket $tiket)
    {
        return view('tiket.show')->with('data', $tiket);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tiket  $tiket
     * @return \Illuminate\Http\Response
     */
    public function edit(Tiket $tiket)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tiket  $tiket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tiket $tiket)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tiket  $tiket
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tiket $tiket)
    {
        //
    }
}
