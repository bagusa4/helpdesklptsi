<?php

namespace App\Http\Controllers\Teknisi;

use App\Http\Controllers\Traits\TiketControllerTrait;
use App\Models\Tiket;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class TiketController extends Controller
{
    use TiketControllerTrait;

    /**
     * TiketController constructor.
     */
    public function __construct()
    {
        $this->middleware('role:Teknisi');
    }

    /**
     * Override this method to get tiket data with spesific teknisi
     *
     * @param Tiket $tiket
     * @return mixed
     */
    protected function getData(Tiket $tiket)
    {
        return $tiket->withTeknisi(Auth::user()->id)->with(['user', 'unitKerja']);
    }

    /**
     * Override this mtehod to get spesific tiket data and with spesific teknisi
     *
     * @param Tiket $tiket
     * @return mixed
     */
    protected function getFind(Tiket $tiket)
    {
        return $tiket->withTeknisi(Auth::user()->id)->with(['kategori', 'user', 'teknisi', 'tiketInfo', 'solution', 'unitKerja']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('teknisi.tiket.tiket');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tiket  $tiket
     * @return \Illuminate\Http\Response
     */
    public function show(Tiket $tiket)
    {
        return view('teknisi.tiket.show')->with('data', $tiket);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tiket  $tiket
     * @return \Illuminate\Http\Response
     */
    public function edit(Tiket $tiket)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tiket  $tiket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tiket $tiket)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tiket  $tiket
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tiket $tiket)
    {
        //
    }
}
