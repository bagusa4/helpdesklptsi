<?php

namespace App\Http\Controllers\Traits;

use App\Models\User;
use App\Models\IdentitasUsers;
use App\Models\Role;
use App\Models\UnitKerja;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use SebastianBergmann\CodeCoverage\Report\Xml\Unit;

trait UsersControllerTrait
{
    use NotifyControllerTrait;

    /**
     * Display all data listing of the resource
     *
     * @param Request $request
     * @param \App\Models\User $user
     * @return $this
     */
    public function data(Request $request, User $user)
    {
        $search = $request->input('search');
        $searchId = $request->input('searchId');
        $orderBy = $request->input('orderBy');
        $customQuery = [
            'search' => $search,
            'searchId' => $searchId,
            'orderBy' => $orderBy
        ];

        $response = $user->withRole($this->withRole())->with('identitas', 'roles', 'unitKerja')
            ->search($search)->findById($searchId)->orderingBy($orderBy)
            ->paginate();
        $response->appends($customQuery)->fragment('table')->links();

        return response()
            ->json($response)
            ->header('Content-Type', 'application/json');
    }

    /**
     * Get all options
     *
     * @return $this
     */
    public function options(Request $request)
    {
        return response()
            ->json($this->getOptions($request))
            ->header('Content-type', 'application/json');
    }

    /**
     * Set the options returns
     *
     * @param $request
     * @return \Illuminate\Support\Collection
     */
    protected function getOptions($request)
    {
        $options = collect([
            'unitKerja' => UnitKerja::all(),
            'roles' => Role::all(),
        ]);

        return $options;
    }

    /**
     * Overide this method to get data with spesific role
     *
     * @return null
     */
    protected function withRole()
    {
        return null;
    }

    /**
     * Check if the action is for users trait
     *
     * @return bool
     */
    protected function users()
    {
        return true;
    }

    /**
     * for validation when has request to store or update
     *
     * @param Request $request
     * @return mixed
     */
    protected function validation(Request $request)
    {
        return Validator::make($request->all(), [
            'get.name' => 'nim_username|max:255',
            'get.email' => 'email|max:255',
            'get.password' => 'min:6',
            'get.active' => 'boolean',
            'get.identitas.nama_lengkap' => 'max:255',
            'get.identitas.tanggal_lahir' => 'max:255',
            'get.identitas.tempat_lahir' => 'max:255',
            'get.identitas.alamat' => 'max:255',
            'get.identitas.status' => 'max:255',
            'get.identitas.jabatan' => 'max:255'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $response = collect();

        $this->validation($request)->validate();

        $updateUser = $user->find($user->id);
        $updateUser->name = $request->input('get.name');
        $updateUser->email = $request->input('get.email');
        if (($password = $request->input('get.password')) !== null) {
            $updateUser->password = bcrypt($password);
        }
        $updateUser->active = $request->input('set.selected.active');
        $updateUser->unit_kerja_id = $request->input('set.selected.unit_kerja_id');

        $updateIdentitas = [
            'nama_lengkap' => $request->input('get.identitas.nama_lengkap'),
            'tanggal_lahir' => $request->input('get.identitas.tanggal_lahir'),
            'tempat_lahir' => $request->input('get.identitas.tempat_lahir'),
            'alamat' => $request->input('get.identitas.alamat'),
            'status' => $request->input('get.identitas.status'),
            'jabatan' => $request->input('get.identitas.jabatan'),
            'auth_user_id' => $updateUser->id
        ];

        if ($updateUser->identitas !== null) {
            $updateUser->identitas->update($updateIdentitas);
        } else {
            $identitas = IdentitasUsers::firstOrCreate($updateIdentitas);
            $identitas->user()->associate($updateUser);
        }

        if ($this->users()) {
            if (count($inputRoles = $request->input('set.selected.roles')) !== 0) {
                $updateUser->roles()->detach();
                $updateUser->roles()->attach($inputRoles);
            }
        }

        $update = $updateUser->save();

        return response()
                ->json(
                    $this->notify(
                        $update,
                        [Lang::get('messages.success.user.update.title'), Lang::get('messages.failed.user.update.title'), $update]
                    )
                )
                ->header('Content-Type', 'application/json');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $destroy = $user->find($user->id)->delete();

        return response()
            ->json(
                $this->notify(
                    $destroy,
                    [Lang::get('messages.success.user.delete.title'), Lang::get('messages.failed.user.delete.title'), $user]
                )
            )
            ->header('Content-Type', 'application/json');
    }
}
