<?php

namespace App\Http\Controllers\Traits;

use App\Models\Tiket;
use App\Models\TiketInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

trait TiketInfoControllerTrait
{
    use NotifyControllerTrait;

    /**
     * get info instance, just for this scope
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    protected function tiketInfo(Tiket $tiket)
    {
        return TiketInfo::withTiket($tiket->id)->with(['tiket', 'user'])->get();
    }

    /**
     * Get data of tiketInfo, get request
     *
     * @return $this
     */
    public function data(Tiket $tiket)
    {
        return response()
                ->json($this->tiketInfo($tiket))
                ->header('Content-Type', 'application/json');
    }

    /**
     * Validation
     *
     * @param array $request
     * @return mixed
     */
    protected function validation(array $request)
    {
        return Validator::make($request, [
            'set.status_info' => 'required|max:255',
            'set.deskripsi_info' => 'required|max:255',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Tiket $tiket)
    {
        $this->validation($request->all())->validate();

        $currentUser = Auth::user();

        $info = new TiketInfo();
        $infoRequest = [
            'status_info' => $request->input('set.status_info'),
            'deskripsi_info' => $request->input('set.deskripsi_info'),
        ];
        $info->fill($infoRequest);
        $info->user()->associate($currentUser);
        $info->tiket()->associate($tiket);

        $save = $info->save();

        return response()
            ->json($this->notify($save, [
                Lang::get('messages.success.tiket.add.info.title'),
                Lang::get('messages.failed.tiket.add.info.title'),
                $info
            ]))
            ->header('Content-type', 'application/json');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tiket @tiket
     * @param  \App\Models\TiketInfo  $info
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tiket $tiket, TiketInfo $info)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tiket @tiket
     * @param  \App\Models\TiketInfo  $info
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tiket $tiket, TiketInfo $info)
    {
        //
    }
}
