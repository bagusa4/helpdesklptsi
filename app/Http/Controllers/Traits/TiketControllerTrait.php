<?php

namespace App\Http\Controllers\Traits;

use App\Models\Tiket;
use App\Models\KategoriTiket;
use App\Models\Teknisi;
use App\Models\UnitKerja;
use Illuminate\Http\Request;
use SebastianBergmann\CodeCoverage\Report\Xml\Unit;

trait TiketControllerTrait
{
    use NotifyControllerTrait;

    /**
     * Override this method to get spesific data
     *
     * @param Tiket $tiket
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    protected function getData(Tiket $tiket)
    {
        return $tiket->with(['user', 'unitKerja']);
    }

    /**
     * Override this method to get spesific find data
     *
     * @param Tiket $tiket
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    protected function getFind(Tiket $tiket)
    {
        return $tiket->with(['kategori', 'user', 'teknisi', 'tiketInfo', 'solution', 'unitKerja']);
    }

    /**
     * Set the options returns
     *
     * @param $request
     * @return \Illuminate\Support\Collection
     */
    protected function getOptions($request)
    {
        $options = collect();

        $request->input('kategori') ? $options['kategori'] = KategoriTiket::orderBy('nama_kategori', 'DESC')->get() : "";
        $request->input('teknisi') ? $options['teknisi'] = Teknisi::all() : "";
        $request->input('unitkerja') ? $options['unitKerja'] = UnitKerja::orderBy('nama', 'DESC')->get() : "";

        return $options;
    }

    /**
     * Get data with filters query request
     *
     * @param $request
     * @param $tiket
     * @return mixed
     */
    protected function getFilters($request, $tiket)
    {
        $search = $request->input('search');
        $searchId = $request->input('searchId');
        $orderBy = $request->input('orderBy');
        $customQuery = [
            'search' => $search,
            'searchId' => $searchId,
            'orderBy' => $orderBy
        ];

        $response = $this->getData($tiket)
            ->search($search)->findById($searchId)->orderingBy($orderBy)
            ->paginate();
        $response->appends($customQuery)->fragment('table')->links();

        return $response;
    }


    /**
     * Display all data listing of the resource
     *
     * @param Tiket $tiket
     * @return $this
     */
    public function data(Request $request, Tiket $tiket)
    {
        return response()
            ->json($this->getFilters($request, $tiket))
            ->header('Content-Type', 'application/json');
    }

    /**
     * Show spesific tiket data
     *
     * @param $id
     * @param Tiket $tiket
     * @return $this
     */
    public function find(Tiket $tiket)
    {
        return response()
            ->json($this->getFind($tiket)->find($tiket->id))
            ->header('Context-Type', 'application/json');
    }

    /**
     * Get Options with query string request to filter data responses
     *
     * @param Request $request
     * @return $this
     */
    public function options(Request $request)
    {
        return response()
                ->json($this->getOptions($request))
                ->header('Content-Type', 'application/json');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tiket  $tiket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tiket $tiket)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tiket  $tiket
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tiket $tiket)
    {
        //
    }
}
