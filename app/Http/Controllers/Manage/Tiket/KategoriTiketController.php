<?php

namespace App\Http\Controllers\Manage\Tiket;

use App\Http\Controllers\Traits\NotifyControllerTrait;
use App\Models\KategoriTiket;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class KategoriTiketController extends Controller
{
    use NotifyControllerTrait;

    /**
     * Do something when constructor called
     */
    public function __construct()
    {
        $this->middleware('role:Admin|Operator');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('manage.tiket.kategori');
    }

    /**
     * Get all kategori data
     *
     * @param KategoriTiket $kategoriTiket
     * @return $this
     */
    public function data(Request $request, KategoriTiket $kategoriTiket)
    {
        $search = $request->input('search');
        $searchId = $request->input('searchId');
        $orderBy = $request->input('orderBy');
        $customQuery = [
            'search' => $search,
            'searchId' => $searchId,
            'orderBy' => $orderBy
        ];

        $response = $kategoriTiket
            ->search($search)->findById($searchId)->orderingBy($orderBy)
            ->paginate(20);
        $response->appends($customQuery)->fragment('table')->links();

        return response()
                ->json($response)
                ->header('Content-Type', 'application/json');
    }

    /**
     * Search Kategori Tiket
     *
     * @param $data
     * @param KategoriTiket $kategoriTiket
     * @return $this
     */
    public function search($data, KategoriTiket $kategoriTiket)
    {
        return response()
                ->json($kategoriTiket->where('nama_kategori', 'LIKE', $data)->get())
                ->header('Content-Type', 'application/json');
    }

    protected function validation(Request $request)
    {
        return Validator::make($request->all(), [
            'nama_kategori' => 'nullable',
            'deskripsi_kategori' => 'nullable'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validation($request)->validate();

        $kategori = new KategoriTiket();
        $kategori->nama_kategori = $request->input('nama_kategori');
        $kategori->deskripsi_kategori = $request->input('deskripsi_kategori');

        $save = $kategori->save();

        return response()
                ->json($this->notify($save, [
                    'Berhasil di tambahkan !!!',
                    'Gagal di tambahkan !!!',
                    $kategori
                ]))->header('Content-Type', 'application/json');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\KategoriTiket  $kategoriTiket
     * @return \Illuminate\Http\Response
     */
    public function show(KategoriTiket $kategoriTiket)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\KategoriTiket  $kategoriTiket
     * @return \Illuminate\Http\Response
     */
    public function edit(KategoriTiket $kategoriTiket)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\KategoriTiket  $kategoriTiket
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request, KategoriTiket $kategoriTiket)
    {
        $this->validation($request)->validate();

        $kategori = $kategoriTiket->find($id);
        $kategori->nama_kategori = $request->input('nama_kategori');
        $kategori->deskripsi_kategori = $request->input('deskripsi_kategori');

        $update = $kategori->save();

        return response()
            ->json($this->notify($update, [
                'Berhasil di update !!!',
                'Gagal di update !!!',
                $kategori
            ]))->header('Content-Type', 'application/json');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\KategoriTiket  $kategoriTiket
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, KategoriTiket $kategoriTiket)
    {
        $destroy = $kategoriTiket->find($id)->delete();

        return response()
            ->json($this->notify($destroy, [
                'Berhasil di hapus !!!',
                'Gagal di hapus !!!',
                $kategoriTiket
            ]))->header('Content-Type', 'application/json');
    }
}
