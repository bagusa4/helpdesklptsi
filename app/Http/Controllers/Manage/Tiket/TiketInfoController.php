<?php

namespace App\Http\Controllers\Manage\Tiket;

use App\Http\Controllers\Traits\TiketInfoControllerTrait;
use App\Models\Tiket;
use App\Models\TiketInfo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TiketInfoController extends Controller
{
    use TiketInfoControllerTrait;

    /**
     * TiketInfoController constructor.
     */
    public function __construct()
    {
        $this->middleware('role:Admin|Operator');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TiketInfo  $tiketInfo
     * @return \Illuminate\Http\Response
     */
    public function show(TiketInfo $tiketInfo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TiketInfo  $tiketInfo
     * @return \Illuminate\Http\Response
     */
    public function edit(TiketInfo $tiketInfo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tiket @tiket
     * @param  \App\Models\TiketInfo  $info
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tiket $tiket, TiketInfo $info)
    {
        $this->validation($request->all())->validate();

        $tiketInfo = $info;
        $updateInfo = [
            'status_info' => $request->input('set.status_info'),
            'deskripsi_info' => $request->input('set.deskripsi_info')
        ];
        $tiketInfo->update($updateInfo);

        $update = $tiketInfo->save();

        return response($this->notify($update, [
            'Berhasil di update !!!',
            'Gagal di update !!!',
            $tiketInfo
        ]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Tiket $tiket
     * @param TiketInfo $info
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tiket $tiket, TiketInfo $info)
    {
        $tiketInfo = $info;
        $delete = $tiketInfo->delete();

        return response($this->notify($delete, [
            'Berhasil di hapus !!!',
            'Gagal di hapus !!!',
            $tiketInfo
        ]));
    }
}
