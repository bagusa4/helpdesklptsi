<?php

namespace App\Http\Controllers\Manage\Tiket;

use App\Http\Controllers\Traits\TiketControllerTrait;
use App\Models\Role;
use App\Models\Tiket;
use App\Models\UnitKerja;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RekapTiketController extends Controller
{
    use TiketControllerTrait;

    public function __construct()
    {
        $this->middleware('role:Admin|Operator');
    }

    /**
     * Override this method to get spesific data
     *
     * @param Tiket $tiket
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    protected function getData(Tiket $tiket)
    {
        $result = $tiket->with(['unitKerja', 'user.roles', 'kategori', 'solution']);

        return $result;
    }

    /**
     * Set the options returns
     *
     * @param $request
     * @return \Illuminate\Support\Collection
     */
    protected function getOptions($request)
    {
        $options = collect();

        $request->input('role') ? $options['role'] = Role::all() : "";
        $request->input('unitKerja') ? $options['unit_kerja'] = UnitKerja::all() : "";

        return $options;
    }

    /**
     * Get data with filters query request
     *
     * @param $request
     * @param $tiket
     * @return mixed
     */
    protected function getFilters($request, $tiket)
    {
        $date = $request->input('date');
        $month = $request->input('month');
        $role = $request->input('role');
        $unitKerja = $request->input('unitKerja');
        $orderBy = $request->input('orderBy');
        $customQuery = [
            'date' => $date,
            'month' => $month,
            'role' => $role,
            'unitKerja' => $unitKerja,
            'orderBy' => $orderBy
        ];

        $response = $this->getData($tiket)
            ->withUserRole($role)->withUnitKerja($unitKerja)->onDate($date)->onMonth($month)->orderingBy($orderBy)
            ->paginate();
        $response->appends($customQuery)->fragment('table')->links();

        return $response;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('manage.tiket.rekap');
    }

    public function pdf(Request $request, Tiket $tiket)
    {
        $data = collect();
        $data['date'] = $request->input('date');
        $data['month'] = $request->input('month');
        $data['role'] = $request->input('role') ? Role::find($request->input('role'))->first() : false;
        $data['unitKerja'] =  $request->input('unitKerja') ? UnitKerja::find($request->input('unitKerja'))->first() : false;
        $data['orderBy'] = $request->input('orderBy');
        $data['filters'] = $this->getFilters($request, $tiket);

        $pdf = PDF::loadView('manage.tiket.pdf.rekap', $data);

        $pdf->setPaper('A4', 'landscape');

        $stream = $pdf->stream();
        //$stream = view('manage.tiket.pdf.rekap', $data);

        return $stream;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tiket  $tiket
     * @return \Illuminate\Http\Response
     */
    public function show(Tiket $tiket)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tiket  $tiket
     * @return \Illuminate\Http\Response
     */
    public function edit(Tiket $tiket)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tiket  $tiket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tiket $tiket)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tiket  $tiket
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tiket $tiket)
    {
        //
    }
}
