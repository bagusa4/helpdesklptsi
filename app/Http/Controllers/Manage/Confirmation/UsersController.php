<?php

namespace App\Http\Controllers\Manage\Confirmation;

use App\Http\Controllers\Traits\NotifyControllerTrait;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Lang;

class UsersController extends Controller
{
    use NotifyControllerTrait;

    /**
     * UsersController constructor.
     */
    public function __construct()
    {
        $this->middleware('role:Admin|Operator');
    }

    /**
     * Get not active users
     *
     * @return $this
     */
    public function data(Request $request)
    {
        $search = $request->input('search');
        $searchId = $request->input('searchId');
        $orderBy = $request->input('orderBy');
        $customQuery = [
            'search' => $search,
            'searchId' => $searchId,
            'orderBy' => $orderBy
        ];

        $response = User::notActive()->with(['roles', 'unitKerja'])
            ->search($search)->findById($searchId)->orderingBy($orderBy)
            ->paginate();
        $response->appends($customQuery)->fragment('table')->links();

        return response()
            ->json($response)
            ->header('Content-Type', 'application/json');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('manage.confirmation.users');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $accept = $user->notActive()->find($user->id);
        $accept->active = true;
        $accept = $accept->save();

        return response()
            ->json($this->notify($accept, [
                Lang::get('messages.success.confirmation.user.accept.title'),
                Lang::get('messages.failed.confirmation.user.accept.title'),
                $user
            ]))
            ->header('Content-type', 'application/json');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $decline = $user->notActive()->find($user->id);
        $decline = $user->forceDelete();

        return response()
            ->json($this->notify($decline, [
                Lang::get('messages.success.confirmation.user.decline.title'),
                Lang::get('messages.failed.confirmation.user.decline.title'),
                $user
            ]))
            ->header('Content-type', 'application/json');
    }
}
