<?php

namespace App\Http\Controllers\Manage;

use App\Models\User;
use App\Models\IdentitasUsers;
use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\UnitKerja;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Auth\Events\Registered;

class UserRegisterController extends Controller
{
    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/manage/allusers';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('role:Admin|Operator');
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        $data = collect([
            'unit' => UnitKerja::all(),
            'role' => Role::all()
        ]);

        return view('manage.user_register')->with('data', $data)->render();
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|nim_username|max:255|unique:users',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'role' => 'required|max:1|not_in:null',
            'active' => 'required',
            'unit_kerja' => 'required|nullable|not_in:null',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'active' => $data['active'],
            'unit_kerja_id' => $data['unit_kerja'],
        ]);

        $identitas = IdentitasUsers::create([
            'auth_user_id' => $user->id
        ]);
        $identitas->user()->associate($user);

        $userSave = $user;
        $user->roles()->attach($data['role']);
        return $userSave;
    }
}
