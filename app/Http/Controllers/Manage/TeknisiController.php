<?php

namespace App\Http\Controllers\Manage;

use App\Http\Controllers\Traits\UsersControllerTrait;
use App\Models\User;
use App\Http\Controllers\Controller;

class TeknisiController extends Controller
{
    use UsersControllerTrait;

    /**
     * Do something when construtor called
     */
    public function __construct()
    {
        $this->middleware('role:Admin|Operator');
    }

    /**
     * Get spesifi data with role Operator
     *
     * @return string
     */
    protected function withRole()
    {
        return 'Teknisi';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('manage.teknisi');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }
}
