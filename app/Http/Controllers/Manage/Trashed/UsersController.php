<?php

namespace App\Http\Controllers\Manage\Trashed;

use App\Http\Controllers\Traits\NotifyControllerTrait;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Lang;

class UsersController extends Controller
{
    use NotifyControllerTrait;

    public function __construct()
    {
        $this->middleware('role:Admin|Operator');
    }

    /**
     * Return the users trashed data
     *
     * @return mixed
     */
    public function data(Request $request)
    {
        $search = $request->input('search');
        $searchId = $request->input('searchId');
        $orderBy = $request->input('orderBy');
        $customQuery = [
            'search' => $search,
            'searchId' => $searchId,
            'orderBy' => $orderBy
        ];

        $response = User::onlyTrashed()->with(['roles', 'unitKerja'])
            ->search($search)->findById($searchId)->orderingBy($orderBy)
            ->paginate();
        $response->appends($customQuery)->fragment('table')->links();

        return response()
                ->json($response)
                ->header('Content-type', 'application/json');
    }

    /**
     * Restore deleted user
     *
     * @param $id
     * @param \App\Models\User $user
     * @return $this
     */
    public function restore($id, User $user)
    {
        $restore = $user->onlyTrashed()->find($id);
        $restore->restore();
        $restore = $restore->save();

        return response()
            ->json($this->notify($restore, [
                Lang::get('messages.success.trashed.user.restore.title'),
                Lang::get('messages.failed.trashed.user.restore.title'),
                $user
            ]))
            ->header('Content-type', 'application/json');
    }

    /**
     * force permantly deleted user
     *
     * @param $id
     * @param \App\Models\User $user
     * @return $this
     */
    public function delete($id, User $user)
    {
        $destroy = $user->onlyTrashed()->find($id);
        $destroy = $destroy->forceDelete();

        return response()
            ->json($this->notify($destroy, [
                Lang::get('messages.success.trashed.user.delete.title'),
                Lang::get('messages.failed.trashed.user.delete.title'),
                $user
            ]))
            ->header('Content-type', 'application/json');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('manage.trashed.users');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {

    }
}
