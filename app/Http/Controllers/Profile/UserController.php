<?php

namespace App\Http\Controllers\Profile;

use App\Models\User;
use App\Models\IdentitasUsers;
use App\Models\UnitKerja;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use SebastianBergmann\CodeCoverage\Report\Xml\Unit;

class UserController extends Controller
{
    /**
     * UserController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('profile.user')->with('user', $this->user());
    }

    /**
     * Get auth user data
     *
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    protected function user()
    {
        return Auth::user();
    }

    /**
     * Validation
     *
     * @param Request $request
     * @return mixed
     */
    protected function validation(Request $request)
    {
        return Validator::make($request->all(),[
            'name' => 'nim_username|max:255',
            'email' => 'email|max:255',
            'unit_kerja' => 'required|nullable|not_in:null',
        ]);
    }

    /**
     * Models change password form
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showChangePasswordForm()
    {
        return view('profile.password');
    }

    /**
     * Models change password action
     *
     * @param Request $request
     * @param \App\Models\User $user
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function changePassword(Request $request, User $user)
    {
        $validation = Validator::make($request->all(), [
            'password' => 'min:6|confirmed',
        ]);
        $validation->validate();

        $user = $user->find($this->user()->id);

        if (($password = $request->input('password')) !== null) {
            $user->password = bcrypt($password);
        }

        $user->save();

        return redirect(url('/user/profile'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $data = collect([
           'user' => $this->user(),
            'unit' => UnitKerja::all()
        ]);

        return view('profile.edit')->with('data', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user, IdentitasUsers $identitasUsers)
    {
        $this->validation($request)->validate();

        $user = $user->find($this->user()->id);
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->unit_kerja_id = $request->input('unit_kerja');

        $identitas = [
            'nama_lengkap' => $request->input('nama_lengkap'),
            'tanggal_lahir' => $request->input('tanggal_lahir'),
            'tempat_lahir' => $request->input('tempat_lahir'),
            'alamat' => $request->input('alamat'),
            'status' => $request->input('status'),
            'jabatan' => $request->input('jabatan'),
        ];

        if (!$user->identitas == null) {
            $user->identitas->update($identitas);
        } else {
            $identitasUser = $identitasUsers->firstOrCreate($identitas);
            $identitasUser->user()->associate($user);
        }

        $user->save();

        return redirect(url('/user/profile'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
}
