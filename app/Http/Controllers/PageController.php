<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;

class PageController extends Controller
{
    public function __construct()
    {
        $this->pdf = App::make('dompdf.wrapper');
    }

    private function storage($type)
    {
        return Storage::disk($type);
    }

    public function dokumentasi()
    {
        $direktori = "files/Penggunaan-Web-HELP-DESK-LPTSI-Unsoed.pdf";
        $storage = $this->storage('public');
        if ($storage->exists($direktori)) {
            $stream = $storage->get($direktori);
        } else {
            return abort(404);
        }
        return Response::make($stream, 200, ['Content-type' => 'application/pdf']);
    }

    public function formulir()
    {
        $formulir = collect();

        for ($i = 0; $i < 20; ++$i) {
            $formulir->push(
                [
                    'judul' => 'Test',
                    'link' => url('/home')
                ]
            );
        }

        return view('page.formulir')->with('data', $formulir)->render();
    }
}
