<?php
/**
 * Created by PhpStorm.
 * User: Bagusa4 - Smecon
 * Date: 4/2/2017
 * Time: 12:48 PM
 */

namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Inspiring as Inspire;

class QuotesViewComposer
{
    private $quotes;

    public function __construct(Inspire $inspire)
    {
        $this->quotes = $inspire->quote();
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('quotes', $this->quotes);
    }

}