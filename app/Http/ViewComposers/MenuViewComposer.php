<?php
/**
 * Created by PhpStorm.
 * User: Bagusa4 - Smecon
 * Date: 4/2/2017
 * Time: 12:45 PM
 */

namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use App\Http\Controllers\Collections\Menu;
use Illuminate\Support\Facades\Auth;

class MenuViewComposer
{
    private $menu;

    public function __construct(Menu $menu)
    {
        $this->menu = $menu->getMenu()->all();
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('menus', $this->menu);
    }
}