const elixir = require('laravel-elixir');
const Task = elixir.Task;

require('laravel-elixir-vue-2');

const shell = require('gulp-shell');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application as well as publishing vendor resources.
 |
 */

/**
 * Elixr
 * Make Some Task
 */
elixir.extend('langjs', function(path) {
    new Task('langjs', function() {
        gulp.src('').pipe(shell('php artisan lang:js ' + (path || 'resources/assets/js/localization/messages.js') + ' --no-lib'));
    });
});

/**
 * Elixir - Main
 */
elixir((mix) => {
    /**
     * Elixr
     * Make Something Helper
     */
    let helper = {
        copy: {
            node_modules: {
                libs: function (path, custom = null) {
                    mix.copy("node_modules/" + path, (custom !== null ? custom : "resources/assets/libs/" + path));
                },
                fonts: function (path, custom = null) {
                    mix.copy("node_modules/" + path, (custom !== null ? custom : "public/assets/fonts/" + path));
                }
            }
        }
    };

    /**
     * Mix Copy
     */
    helper.copy.node_modules.fonts("font-awesome/fonts/", "public/assets/fonts/");

    /**
     * Mix Sass
     */
    mix.sass('app.scss', 'resources/assets/css/appSass.css');

    /**
     * Mix Less
     */
    mix.less('app.less', 'resources/assets/css/appLess.css');

    /**
     * Mix Combine
     */
    mix.combine([
        'resources/assets/css/appSass.css',
        'resources/assets/css/appLess.css',
        'node_modules/animate.css/animate.css'
    ], 'public/css/app.css');

    /**
     * Mix JS
     */
    mix.langjs();
    mix.webpack('app.js');
});
