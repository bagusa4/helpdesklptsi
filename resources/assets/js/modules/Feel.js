/**
 * Created by Bagusa4 on 2/21/2017.
 */

class Event {
    constructor (vue, options)
    {
        this.vue = new Vue();
        this.options = options;
        this.events = [];
    }

    /** Vue Default Events **/
    $on(_name, _object)
    {
        this.vue.$on(_name, _object);
    }

    $emit(_name, _arguments = null)
    {
        this.vue.$emit(_name, _arguments);
    }

    $once(_name, _object)
    {
        this.vue.$once(_name, _object);
    }

    $off(_name = null)
    {
        this.vue.$off(_name);
    }

    /** Feel Event **/
    add(_name, _object)
    {
        this.events[_name] = this.events[_name] || [];
        this.events[_name].push(_object);
    }

    fire(_name, _arguments = null)
    {
        if (this.events[_name]) {
            this.events[_name].forEach(function (fn) {
                fn(_arguments);
            })
        }
    }

    remove(_name)
    {
        let index = this.events.indexOf(_name);
        if (index > -1) {
            this.events.splice(index, 1);
        }
    }

    removes()
    {
        this.events = [];
    }

    all()
    {
        return this.events
    }
}

const Feels = function (Vue, options) {
    this.vue = new Vue();
    this.options = options;
    this.event = new Event(Vue, options);
}

const Feel = {
    install(Vue, options)
    {
        Vue.feel = new Feels(Vue, options);
        Vue.prototype.$feel = Vue.feel;
    }
}

if (typeof window !== 'undefined' && window.Vue) {
    window.Vue.use(Feel)
}

export default Feel