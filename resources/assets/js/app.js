
/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('./appcustom');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

/** Localization - LangJs **/
import Lang from './localization/lang';
Lang.setLocale('id');
Vue.prototype.$lang = Lang;

/** Vue Feel Plugin **/
import Feel from './modules/Feel';
Vue.use(Feel);

/* Init Vuex */
import Vuex from 'vuex';
Vue.use(Vuex);

/* Modules */
import { dataTable } from './modules/DataTable';

const store = new Vuex.Store({
    state: {
        count: 0
    },
    getters: {
        hello: (state) => {
            return state.count
        }
    },
    mutations: {
        increment (state) {
            state.count++
        }
    },
    action: {

    },
    modules: {
        dataTable
    }
});


/**
 *  Components
 */
import myMenu from './components/MyMenu.vue';

/**
 *  Manage
 */
import manageAllUsers from './components/manage/AllUsers.vue';
import manageAdmin from './components/manage/Admin.vue';
import manageOperator from './components/manage/Operator.vue';
import manageTeknisi from './components/manage/Teknisi.vue';
import manageUser from './components/manage/User.vue';
import manageUnitKerja from './components/manage/UnitKerja.vue';
import manageConfirmationUsers from './components/manage/confirmation/Users.vue';
import manageTrashedUsers from './components/manage/trashed/Users.vue';

import manageTiket from './components/manage/tiket/Tiket.vue';
import manageTiketShow from './components/manage/tiket/Show.vue';
import manageTiketKategori from './components/manage/tiket/Kategori.vue';
import manageTiketRekap from './components/manage/tiket/Rekap.vue';

import teknisiTiket from './components/tiket/teknisi/Tiket.vue';
import teknisiTiketShow from './components/tiket/teknisi/Show.vue';

import tiketCreate from './components/tiket/Create.vue';
import Tiket from './components/tiket/Tiket.vue';
import tiketShow from './components/tiket/Show.vue';
import TiketKu from './components/tiket/ku/Tiket.vue';
import TiketKuShow from './components/tiket/ku/Show.vue';

const app = new Vue({
    el: '#app',
    store,
    components: {
        myMenu,

        manageAllUsers,
        manageAdmin,
        manageOperator,
        manageTeknisi,
        manageUser,
        manageUnitKerja,
        manageConfirmationUsers,
        manageTrashedUsers,

        manageTiket,
        manageTiketShow,
        manageTiketKategori,
        manageTiketRekap,

        teknisiTiket,
        teknisiTiketShow,

        tiketCreate,
        Tiket,
        tiketShow,
        TiketKu,
        TiketKuShow
    },
    data: {
        Auth: window.Auth
    },
    methods: {
        notify: function (title, message, status) {
            if (status) {
                $.notify({
                    title: "<strong>" + title + "</strong>",
                    message: message,
                }, {
                    type: 'success'
                })
            } else {
                $.notify({
                    title: "<strong>" + title + "</strong>",
                    message: message,
                }, {
                    type: 'warning'
                })
            }
        },
        filterNulls (obj) {
            var isArray = obj instanceof Array;
            for (var k in obj) {
                if (obj[k] === null) obj[k] = "";
                else if (typeof obj[k] == "object") this.filterNulls(obj[k]);
                if (isArray && obj.length == k) this.filterNulls(obj);
            }
            return obj;
        },
        redirectTo(url) {
            window.location.href = url
        },
        getAuth() {
            return this.$data.Auth
        }
    },
    mounted() {
    }
});