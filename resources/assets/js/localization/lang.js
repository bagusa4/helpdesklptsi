/**
 * Created by Bagusa4 - Smecon on 3/29/2017.
 */

/**
 * Karena isu yang ada belum fix, jadi menggunakan cara manual ,
 * install paket composer Laravel-Js-Localization
 * install paket npm Lang.js (standalone)
 * build message menggunakan paket composernya,
 * lalu buat instance baru dari class Lang.js
 *
 * Referensi => https://github.com/rmariuzzo/Laravel-JS-Localization/issues/89#issuecomment-289270853
 */

import Lang from 'lang.js';
import messages from './messages';

const lang = new Lang({
    messages
});

export default lang;