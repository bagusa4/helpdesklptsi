/**
 * Created by Bagusa4 on 2/1/2017.
 */

window.RandomColor = require('randomcolor');
require('bootstrap-notify');
require('typeahead.js');
require('bootstrap-tagsinput');

$.notifyDefaults({
    mouse_over: 'pause',
    enter: "animated fadeInUp",
    exit: "animated fadeOutDown",
    url_target: '_self',
    z_index: 9999
});