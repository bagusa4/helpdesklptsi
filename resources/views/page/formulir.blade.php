@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Daftar Download Formulir</div>
                <div class="panel-body">
                    <table class="table table-bordered table-striped table-hover">
                        <thead>
                            <th>No</th>
                            <th>Judul</th>
                            <th>Link</th>
                        </thead>
                        <tbody>
                            @foreach($data as $formulir)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td><a href="{{ $formulir['link'] }}" target="_blank">{{ $formulir['judul'] }}</a></td>
                                    <td><a href="{{ $formulir['link'] }}" target="_blank">{{ $formulir['link'] }}</a></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection