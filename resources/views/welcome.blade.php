<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > b, a, span {
                color: #636b6f;
                padding: 0 10px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .links > .quotes {
                font-size: 10px;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    <span class="quotes">{{ $quotes }}</span>
                    @if (Auth::check())
                        <a href="{{ url('/home') }}"><b>@lang('alias.menu.home')</b></a>
                        <a href="{{ url('/logout') }}"
                            onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                            <b>@lang('alias.menu.logout')</b>
                        </a>

                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    @else
                        <a href="{{ url('/login') }}"><b>@lang('alias.menu.login')</b></a>
                    @endif
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    {{ config('app.name', 'Laravel') }}
                </div>

                <div class="links">
                    <!--<a href="https://laravel.com/docs">Documentation</a>
                    <a href="https://laracasts.com">Laracasts</a>
                    <a href="https://laravel-news.com">News</a>
                    <a href="https://forge.laravel.com">Forge</a>
                    <a href="https://github.com/laravel/laravel">GitHub</a>-->
                    <a href="http://unsoed.ac.id">Unsoed</a>
                    <a href="http://lptsi.unsoed.ac.id">LPTSI Unsoed</a>
                    <a href="http://hotspot.unsoed.ac.id">Hotspot Unsoed</a>
                    <a href="http://helpdesk.unsoed.ac.id">Help Desk Unsoed</a>
                    <a href="{{ url('page/formulir') }}">Download Formulir</a>
                    <a href="{{ url('/tiket') }}">@lang('alias.tiket.faq.faq')</a>
                    <a href="{{ url('/page/dokumentasi') }}">@lang('alias.menu.user_manual')</a>
                </div>

                <br><br/>
                <marquee>
                    <span>
                    <strong>
                        <b>@lang('messages.welcome')</b>
                    </strong>
                    </span>
                </marquee>
            </div>
        </div>
    </body>
</html>
