<!DOCTYPE html>
<html>
<head>
    <title>{{ config('app.name', 'Laravel') }} Unsoed - LAPORAN TIKET HELP DESK LPTSI UNSOED</title>
    <style>
        .header {
            text-align: center;
        }
        .table {
            width: 100%;
        }
        .table td:first-child {
            text-align: left;
            width: 20%;
        }
        .table td:nth-child(2) {
            width: 2%;
            text-align: center;
        }
        .table td:nth-child(3) {
            text-align: left;
            width: 80%;
        }
        .khusus {
            width: 100%;
        }
        .khusus tr:nth-child(3) {
            height: 100px;
        }
        .khusus td:first-child {
            text-align: left;
            width: auto;
        }
        .khusus td:nth-child(2) {
            text-align: center;
            width: auto;
        }
        .khusus td:nth-child(3) {
            text-align: right;
            width: auto;
        }
    </style>
</head>
<body>
<div class="header">
    <h1>{{ config('app.name', 'Laravel') }}</h1>    
    <span>Universitas Jenderal Soedirman</span>
    <h2>FORM LAPORAN KERUSAKAN</h2>
    <h4>(Off Line)</h4>
</div>
<hr>
<div class="user">
    <table class="table">
        <tr>
            <th>DATA USER :</th>
            <th></th>
            <th></th>
        </tr>
        <tr>
            <td>NIM/USERNAME</td>
            <td>:</td>
            <td>{{ $nama_pelapor }}</td>
        </tr>
        <tr>
            <td>UNIT KERJA</td>
            <td>:</td>
            <td>{{ $unit_kerja['nama'] }} - {{ $unit_kerja['deskripsi'] }}</td>
        </tr>
        <tr>
            <td>KERUSAKAN</td>
            <td>:</td>
            <td>
                {{ $nama_tiket }}
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>{{ $deskripsi_tiket }}</td>
        </tr>
    </table>
</div>
<hr>
<div class="ekskalasi">
    <table class="table">
        <tr>
            <th>EKSKALASI :</th>
            <th></th>
            <th></th>
        </tr>
        <tr>
            <td>TEKNISI</td>
            <td>:</td>
            <td>@foreach($teknisi as $tek){{ $tek['name'] }}, @endforeach</td>
        </tr>
        <tr>
            <td>ANALISIS KESALAHAN</td>
            <td>:</td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            @if($solution['analisis'] === null)
                <td>_____________________________</td>
            @else
                <td>{{ $solution['analisis'] }}</td>
            @endif
        </tr>
        <tr>
            <td>SOLUSI</td>
            <td>:</td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            @if($solution['solusi'] === null)
                <td>_____________________________</td>
            @else
                <td>{{ $solution['solusi'] }}</td>
            @endif
        </tr>
    </table>
</div>
<hr>
<div class="pertanyaan">
    <table class="khusus">
        <tr>
            <th>PERNYATAAN :</th>
            <th></th>
            <th></th>
        </tr>
        <tr>
            <td rowspan="3" colspan="3" style="text-align: justify;">
                <div>
                    Dengan ini menyatakan bahwa kerusakan
                    seperti yang di laporkan telah selesai diperbaiki
                    pada tanggal ____________________ oleh teknisi LPTSI.
                </div>
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td rowspan="3">TEKNISI</td>
            <td></td>
            <td rowspan="3">USER/PENGGUNA</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>
                <br/>
                <br/>
                <br/>
                <br/>
                ___________________
            </td>
            <td rowspan="3">MENGETAHUI</td>
            <td>
                <br/>
                <br/>
                <br/>
                <br/>
                ___________________
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>
                @foreach($teknisi as $tek)
                    <input type="radio">{{ $tek['name'] }}<br/>
                @endforeach
            </td>
            <td>
                ____________________ *)
            </td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td colspan="3"><br/><br/>KET: *) ATASAN LANGSUNG PENGGUNA/USER</td>
        </tr>
    </table>
</div>
</body>
</html>