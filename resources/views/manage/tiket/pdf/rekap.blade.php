<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <style>
        .align-center { text-align: center }
        #cop {
        }
        #cop .logo {
            float: left;
            margin: 4% auto;
        }
        .title {
            line-height: 8px;
            font-size: large;
        }
        .filters {
            width: 50%;
        }
        #tableRekap {

        }
        .break-now {
            page-break-inside:avoid; page-break-after:always;
        }
        .table {
            color: #333; /* Lighten up font color */
            font-family: Helvetica, Arial, sans-serif; /* Nicer font */
            width: 100%;
            border-collapse:
                    collapse; border-spacing: 0;
        }
        .table td, th {
            border: 1px solid #CCC; height: 30px;
        } /* Make cells a bit taller */
        .table th {
            background: #F3F3F3; /* Light grey background */
            font-weight: bold; /* Make sure they're bold */
        }
        .table td {
            background: #FAFAFA; /* Lighter grey background */
            text-align: center; /* Center our text */
        }
        .table td {
            /*white-space:nowrap;*/
            /*text-overflow: ellipsis;*/
            overflow: hidden;
            max-width: 100%;
        }
        .table td:nth-child(4), td:nth-child(5), td:nth-child(6) {
            /*box-sizing: border-box;*/
            max-width: 25%;
        }
    </style>
</head>
<body>
<div style="max-width:100%; max-height:100%;">
    <div class="align-center" id="cop">
        <img class="logo" src="{{ public_path('/storage/images/LUnsoed.png') }}" width="100" height="100">
        <h2>
            KEMENTRIAN RISET, TEKNOLOGI, DAN PENDIDIKAN TINGGI<br/>
            UNIVERSITAS JENDRAL SOEDIRMAN<br/>
            LEMBAGA PENGEMBANGAN TEKNOLOGI DAN SISTEM INFORMASI
        </h2>
        <h4>
            JL.Prof. dr. Hr. BoenjaminNomor 708 Kotak Pos 115 Purwokerto KodePos 53122<br/>
            Telepon (0281) 635292 (Hunting) Ext. 136, 625228 Faximile 631802<br/>
            Laman: <a href="http://lptsi.unsoed.ac.id">http://lptsi.unsoed.ac.id</a>
        </h4>
    </div>
    <hr/>
    <div class="title align-center">
        <p><b>Lampiran Rekap Data Laporan Kerusakan</b></p>
        <span>IT - UNSOED</span>
    </div>
    <div class="filters">
        <table>
            <tbody>
            <tr>
                <td>@lang('alias.date.date')</td>
                <td>:</td>
                <td>{{ $date }} | </td>
                <td>@lang('alias.name.work_unit')</td>
                <td>:</td>
                <td>{{ $unitKerja['nama'] }} - {{ $unitKerja['deskripsi'] }}</td>
            </tr>
            <tr>
                <td>@lang('alias.date.month')</td>
                <td>:</td>
                <td>{{ $month }} | </td>
                <td>@lang('form.filter.order_by.order_by')</td>
                <td>:</td>
                <td>{{ $orderBy }}</td>
            </tr>
            <tr>
                <td>@lang('alias.name.role')</td>
                <td>:</td>
                <td>{{ $role['name'] }} - {{ $role['description'] }} | </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div id="tableRekap">
        <table class="table">
            <thead class="thead">
                <tr>
                    <th class="th">#No</th>
                    <th class="th">@lang('form.tiket.id')</th>
                    <th class="th">@lang('form.tiket.unit_kerja.title')</th>
                    <th class="th">@lang('form.tiket.kategori.title')</th>
                    <th class="th">@lang('form.tiket.deskripsi.title')</th>
                    <th class="th">@lang('form.tiket.kesimpulan.solusi.title')</th>
                    <th class="th">@lang('form.tiket.prioritas.title')</th>
                    <th class="th">@lang('form.tiket.created_at.title')</th>
                    <th class="th">@lang('form.tiket.updated_at.title')</th>
                </tr>
            </thead>
            <tbody class="tbody">
                <?php $index = 0; ?>
                @foreach($filters['data'] as $tiket)
                    <?php $index++; ?>
                    <tr class="tr">
                        <td class="td" data-th="#No">{{ $index }}</td>
                        <td class="td" data-th="@lang('form.tiket.id')">{{ $tiket['id'] }}</td>
                        <td class="td" data-th="@lang('form.tiket.unit_kerja.title')">{{ $tiket['unit_kerja']['nama'] }} - {{ $tiket['unit_kerja']['deskripsi'] }}</td>
                        <td class="td" data-th="@lang('form.tiket.kategori.title')">
                            @foreach($tiket['kategori'] as $kategori)
                                {{ $kategori['nama_kategori'] }},
                            @endforeach
                        </td>
                        <td class="td" data-th="@lang('form.tiket.deskripsi.title')">{{ $tiket['deskripsi_tiket'] }}</td>
                        <td class="td" data-th="@lang('form.tiket.kesimpulan.solusi.title')">{{ $tiket['solution']['solusi'] }}</td>
                        <td class="td" data-th="@lang('form.tiket.prioritas.title')">{{ $tiket['prioritas_tiket'] }}</td>
                        <td class="td" data-th="@lang('form.tiket.created_at.title')">{{ $tiket['created_at'] }}</td>
                        <td class="td" data-th="@lang('form.tiket.updated_at.title')">{{ $tiket['updated_at'] }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<script type="text/php">
    if ( isset($pdf) ) {
        $x = 760;
        $y = 14;
        $text = "Page {PAGE_NUM} of {PAGE_COUNT}";
        $font = $fontMetrics->getFont("helvetica", "bold");
        $size = 10;
        $pdf->page_text($x, $y, $text, $font, $size, array(0,0,0));
    }
</script>
</body>
</html>