@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">@lang('alias.profile.update.detail')</div>
                    <div class="panel-body">
                        <form id="edit-user-profile" class="form-horizontal" role="form" method="post"
                              action="{{ url('/user/profile/edit') }}">
                            {{ csrf_field() }}
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">@lang('form.user.username.title')</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name" value="{{ $data['user']->name }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">@lang('form.user.email.title')</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ $data['user']->email }}" required>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="nama_lengkap" class="col-md-4 control-label">@lang('form.user.full_name.title')</label>
                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="nama_lengkap" value="{{ $data['user']->identitas->nama_lengkap }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="tanggal_lahir" class="col-lg-4 control-label">@lang('form.user.birth_date.title')</label>
                                <div class="col-md-6">
                                    <input id="tangal_lahir" type="date" class="form-control" name="tanggal_lahir" value="{{ $data['user']->identitas->tanggal_lahir }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="tempat_lahir" class="col-lg-4 control-label">@lang('form.user.birth_place.title')</label>
                                <div class="col-md-6">
                                    <input id="tempat_lahir" type="text" class="form-control" name="tempat_lahir" value="{{ $data['user']->identitas->tempat_lahir }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="alamat" class="col-lg-4 control-label">@lang('form.user.address.title')</label>
                                <div class="col-md-6">
                                    <input id="alamat" type="text" class="form-control" name="alamat" value="{{ $data['user']->identitas->alamat }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="status" class="col-lg-4 control-label">@lang('form.user.status.title')</label>
                                <div class="col-md-6">
                                    <input id="status" type="text" class="form-control" name="status" value="{{ $data['user']->identitas->status }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="jabatan" class="col-lg-4 control-label">@lang('form.user.chair.title')</label>
                                <div class="col-md-6">
                                    <input id="jabatan" type="text" class="form-control" name="jabatan" value="{{ $data['user']->identitas->jabatan }}">
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('unit_kerja') ? ' has-error' : '' }}">
                                <label for="unit-kerja" class="col-md-4 control-label">@lang('form.user.work_unit.title')</label>

                                <div class="col-md-6">
                                    <select name="unit_kerja" class="form-control" required>
                                        <option value="null">@lang('form.user.work_unit.select')</option>
                                        @foreach($data['unit'] as $unit)
                                            @if($data['user']->unitKerja->id === $unit->id)
                                                <option value="{{  $unit['id'] }}" selected>{{ $unit['nama'] }} - {{ $unit['deskripsi'] }}</option>
                                            @else
                                                <option value="{{  $unit['id'] }}">{{ $unit['nama'] }} - {{ $unit['deskripsi'] }}</option>
                                            @endif
                                        @endforeach
                                    </select>

                                    @if ($errors->has('unit_kerja'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('unit_kerja') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="panel-footer">
                        <button type="submit" class="btn btn-info"
                                onclick="event.preventDefault();
                                document.getElementById('edit-user-profile').submit();">@lang('button.confirmation.save')</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection