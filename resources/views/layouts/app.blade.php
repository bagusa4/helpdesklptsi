<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">

    <!-- Scripts -->
    <script type="text/javascript">
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
        @if(!Auth::guest())
        window.Auth = {
            user: {!! Auth::user() !!},
            roles: {!! Auth::user()->roles !!}
        }
        @endif
    </script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ url('/page/dokumentasi') }}">@lang('alias.menu.user_manual')</a></li>
                            <li><a href="{{ url('/login') }}">@lang('alias.menu.login')</a></li>
                        @else
                            <li>
                                <a href="{{ url('/home') }}">
                                    <i class="fa fa-home" aria-hidden="true"></i> @lang('alias.menu.home')
                                </a>
                            </li>
                            @role((['Admin', 'Operator']))
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                        <i class="fa fa-folder-open-o" aria-hidden="true"></i> @lang('alias.menu.main.manage') <span class="caret"></span>
                                    </a>

                                    <ul class="dropdown-menu" role="menu">
                                        @foreach($menus['manage'] as $menu)
                                            <li>
                                                <a href="{{ $menu['url'] }}">{{ $menu['name'] }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>
                            @endrole
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    <i class="fa fa-fax" aria-hidden="true"></i> @lang('alias.menu.main.tiket') <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    @foreach($menus['tiket'] as $menu)
                                        <li>
                                            <a href="{{ $menu['url'] }}">{{ $menu['name'] }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    <i class="fa fa-user-circle-o" aria-hidden="true"></i> {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li class="dropdown-header">
                                        <i class="fa fa-id-card-o" aria-hidden="true"></i> @lang('alias.menu.main.profile')
                                    </li>
                                    @foreach($menus['profile'] as $menu)
                                        <li>
                                            <a href="{{ $menu['url'] }}">{{ $menu['name'] }}</a>
                                        </li>
                                    @endforeach
                                    <li role="separator" class="divider"></li>
                                    <li>
                                        <a href="{{ url('/logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            @lang('alias.menu.logout')
                                        </a>

                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>

    @if (!Auth::guest())
        <footer class="footer">
            <div class="container-fluid">
                <p class="text-muted">
                    <i class="fa fa-copyright" aria-hidden="true"></i> PKL 17 -
                    <a href="http://www.smkn1purwokerto.sch.id">SMK Negeri 1 Purwokerto
                        <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                    </a> - Made with <i class="fa fa-heart fa-col-pink" aria-hidden="true"></i> |
                    <i class="fa fa-quote-left" aria-hidden="true"></i>
                    <span>{{ $quotes }}</span>
                    <i class="fa fa-quote-right" aria-hidden="true"></i>
                </p>
            </div>
        </footer>
    @endif

    <!-- Scripts -->
    <script src="/js/app.js"></script>
</body>
</html>
