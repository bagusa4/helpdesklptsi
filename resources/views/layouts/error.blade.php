<!DOCTYPE html>
<html>
<head>
    <title>@yield('title')</title>

    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

    <style>
        html, body {
            height: 100%;
        }

        body {
            margin: 0;
            padding: 0;
            width: 100%;
            color: #B0BEC5;
            display: table;
            font-weight: 100;
            font-family: 'Lato', sans-serif;
        }

        .container {
            text-align: center;
            display: table-cell;
            vertical-align: middle;
        }

        .content {
            text-align: center;
            display: inline-block;
        }

        .title {
            font-size: 72px;
            margin-bottom: 40px;
        }
    </style>

    <script type="text/javascript">
        (function () {
            var redirectCount = 0;
            var timeout = 4;
            var counter = timeout;

            if (sessionStorage.getItem('errorRedirect')) {
                redirectCount = sessionStorage.getItem('errorRedirect');
            }
            sessionStorage.setItem('errorRedirect', ++redirectCount);

            setInterval(function () {
                counter--;
                window.document.getElementById('counter').innerHTML = counter.toString();
            }, 1000);

            setTimeout(function () {
                if ((counter < 0) || (redirectCount >= 2)) {
                    sessionStorage.removeItem('errorRedirect');
                    window.location.replace("{{ url('/') }}");
                } else {
                    sessionStorage.setItem('errorRedirect', 0);
                    window.history.back();
                }
            }, timeout * 1000);
        })();
    </script>
</head>
<body>
<div class="container">
    <div class="content">
        <div class="title"><a href="{{ url('/') }}">@yield('title')</a></div>
        <strong><span>Will be redirect back after - <span id="counter"></span>s.</span></strong>
        <p><strong><span>{{ $exception->getMessage() }}</span></strong></p>
    </div>
</div>
</body>
</html>