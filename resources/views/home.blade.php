@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-md-8 col-md-offset-2">
                <blockquote>
                    <p>{{ __('messages.dashboard', ['user' => Auth::user()->name]) }}</p>
                </blockquote>

                <div class="panel panel-default">
                    <div id="headPanelStatistik" class="panel-heading" role="tab"
                         style="cursor: pointer;"
                         data-toggle="collapse" data-target="#collapseStatistik"
                         aria-expanded="false" aria-controls="collapseStatistik">
                        <i class="fa fa-bar-chart-o" aria-hidden="true"></i>
                        @lang('alias.menu.main.statistik')
                    </div>
                    <div id="collapseStatistik" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headPanelStatistik">
                        <table class="table table-striped table-hover table-responsive">
                            <thead>
                                <th>@lang('alias.statistik.no')</th>
                                <th>@lang('alias.statistik.nama')</th>
                                <th>@lang('alias.statistik.jumlah')</th>
                            </thead>
                            <tbody>
                                @foreach($data['statistik'] as $statistik)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $statistik['nama'] }}</td>
                                        <td>{{ $statistik['jumlah'] }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                @role((['Admin', 'Operator']))
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-folder-open-o" aria-hidden="true"></i> @lang('alias.menu.main.manage')
                        </div>
                        <div class="panel-body">
                            @foreach($data['menu']['manage'] as $menu)
                                <my-menu :my-menu='{!!json_encode($menu)!!}'></my-menu>
                            @endforeach
                        </div>
                    </div>
                @endrole

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-fax" aria-hidden="true"></i> @lang('alias.menu.main.tiket')
                    </div>
                    <div class="panel-body">
                        @foreach($data['menu']['tiket'] as $menu)
                            <my-menu :my-menu='{!!json_encode($menu)!!}'></my-menu>
                        @endforeach
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-id-card-o" aria-hidden="true"></i> @lang('alias.menu.main.profile')
                    </div>
                    <div class="panel-body">
                        @foreach($data['menu']['profile'] as $menu)
                            <my-menu :my-menu='{!!json_encode($menu)!!}'></my-menu>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
