<?php
/**
 * Created by PhpStorm.
 * User: Bagusa4 - Smecon
 * Date: 4/2/2017
 * Time: 7:07 PM
 */

return [
    /*
    |--------------------------------------------------------------------------
    | Button - button
    |--------------------------------------------------------------------------
    |
    | Nama dari setiap button
    |
    */

    'refresh' => 'Refresh/Segarkan',
    'print' => 'Cetak / PDF',
    'filter' => 'Filters',
    'login' => 'Masuk',
    'logout' => 'Keluar',
    'reset' => [
        'reset' => 'Reset',
        'form' => 'Reset Form',
    ],
    'confirmation' => [
        'yes' => 'Ya',
        'no' => 'Tidak',
        'cancel' => 'Batal',
        'save' => 'Simpan',
        'delete' => 'Hapus',
        'update' => 'Ubah',
        'accept' => 'Terima',
        'decline' => 'Tolak',
    ],
    'restore' => [
        'restore' => 'Kembalikan',
        'user' => 'Kembalikan Pengguna',
    ],
    'forgot' => [
        'password' => 'Lupa Password kamu ??'
    ],
    'register' => [
        'register' => 'Daftar',
        'user' => 'Klik register untuk mendaftar'
    ],
    'create' => [
        'tiket' => 'Buat Laporan/Keluhan',
        'user' => 'Buat User Baru',
    ],
    'submit' => [
        'submit' => 'Submit',
        'tiket' => 'Kirim Laporan/Keluhan'
    ]
];