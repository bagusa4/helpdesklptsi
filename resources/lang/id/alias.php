<?php
/**
 * Created by PhpStorm.
 * User: Bagusa4 - Smecon
 * Date: 3/27/2017
 * Time: 12:23 PM
 */

return [
    /*
    |--------------------------------------------------------------------------
    | Nama Umum Yang digunakan dalam aplikasi ini
    |--------------------------------------------------------------------------
    |
    | Nama - nama umum yang digunakan di aplikasi ini
    |
    */

    /** Umum dan samar */
    'name' => [
        'tiket' => 'Laporan / Keluhan',
        'work_unit' => 'Unit Kerja / Fakultas',
        'role' => 'Hak Akses',
    ],

    'date' => [
        'month' => 'Bulan',
        'week' => 'Minggu',
        'date' => 'Tanggal'
    ],

    'table' => [
        'action' => "Tindakan"
    ],

    'menu' => [
        'home' => 'Home',
        'add' => 'Tambah',
        'user_manual' => 'Dokumentasi Pengguna',
        'register' => 'Daftar',
        'login' => 'Masuk',
        'logout' => 'Keluar',
        'main' => [
            'statistik' => 'Statistik',
            'manage' => 'Manage',
            'tiket' => 'Laporan/Keluhan',
            'profile' => 'User Profile'
        ]
    ],

    /**
     * Home - Bagian Statistik
     */
    'statistik' => [
        'no' => 'No',
        'nama' => 'Nama',
        'jumlah' => 'Jumlah',
        'manage' => [
            'all_user' => 'Jumlah Total All Users',
            'user' => 'Jumlah User Umum',
            'teknisi' => 'Jumlah Teknisi',
            'operator' => 'Jumlah Operator',
            'admin' => 'Jumlah Admin',
            'confirmation' => 'Jumlah User Belum Terkonfirmasi',
            'trashed' => 'Jumlah User Terhapus',
            'unit_kerja' => 'Jumlah Unit Kerja / Fakultas',
        ],
        'tiket' => [
            'tiket' => 'Jumlah Laporan/Keluhan',
            'kategori' => 'Jumlah Kategori Laporan',
            'ku' => 'Jumlah Laporan/Keluhan Ku',
            'faq' => 'Jumlah Total FAQ'
        ]
    ],

    /**
     * Tiket
     */
    'tiket' => [
        'tiket' => "Laporan/Keluhan",
        'detail' => "Detail Laporan/Keluhan",
        'manage' => [
            'manage' => 'Manage Laporan/Keluhan',
            'detail' => 'Managemen Keluhan'
        ],
        'work' => [
            'work' => 'Kerjaan Ku',
            'detail' => 'Lihat Yang Harus Ditangani'
        ],
        'ku' => [
            'ku' => 'Laporan/Keluhan Ku',
            'detail' => 'Detail Laporan/Keluhan Ku',
            'create' => [
                'create' => 'Buat Laporan/Keluhan',
                'detail' => 'Ajukan keluhan kamu agar kami bisa membantu :)',
            ],
        ],
        'rekap' => [
            'rekap' => 'Rekap Laporan/Keluhan',
            'detail' => 'Merekap Data Laporan / Keluhan'
        ],
        'kategori' => [
            'kategori' => 'Kategori Laporan/Keluhan',
            'detail' => 'Mengatur Kategori Laporan/Keluhan',
            'add' => 'Tambah Kategori',
            'modal' => [
                'update' => 'Update Kategori Laporan/Keluhan',
                'delete' => 'Hapus Kategori Laporan/Keluhan'
            ]
        ],
        'faq' => [
            'faq' => 'FAQ Laporan/Keluhan',
            'detail' => 'Lihat keluhan yang ada sebelum anda melapor :)'
        ],
        'create' => [
            'create' => 'Ajukan Laporan',
            'detail' => 'Submit / Ajukan Keluhan Atau Laporan'
        ],
        'show' => [
            'kesimpulan' => [
                'kesimpulan' => 'Kesimpulan'
            ],
            'info' => [
                'info' => 'Tiket Info',
                'modal' => [
                    'update' => 'Update Info',
                    'delete' => 'Delete Info'
                ]
            ],
            'modal' => [
                'kategori' => [
                    'kategori' => 'Ubah Kategori',
                    'id' => 'ID',
                    'nama' => 'Nama',
                    'deskripsi' => 'Deskripsi'
                ],
                'teknisi' => [
                    'teknisi' => 'Ubah Teknisi',
                    'id' => 'ID',
                    'nama' => 'Nama'
                ],
                'delete' => [
                    'delete' => 'Delete Laporan/Keluhan'
                ]
            ]
        ]
    ],

    /*
     * Menu Manage
     */
    'manage' => [
        'manage' => 'Manage',
        'user' => [
            'user' => 'User',
            'detail' => 'Manage User',
            'all' => [
                'all' => 'All Users',
                'detail' => 'Manage All Users',
            ],
            'teknisi' => [
                'teknisi' => 'Teknisi',
                'detail' => 'Manage Teknisi',
            ],
            'operator' => [
                'operator' => 'Operator',
                'detail' => 'Manage Operator',
            ],
            'admin' => [
                'admin' => 'Admin',
                'detail' => 'Manage Admin',
            ],
            'modal' => [
                'update' => [
                    'title' => 'Update Data Pengguna',
                    'detail' => ''
                ],
                'delete' => [
                    'title' => 'Delete Pengguna',
                    'detail' => ''
                ]
            ]
        ],
        'confirmation' => [
            'user' => [
                'user' => 'Konfirmasi User',
                'detail' => 'Konfirmasi User Baru',
                'modal' => [
                    'pending' => [
                        'title' => 'User Pending',
                        'detail' => ''
                    ],
                    'decline' => [
                        'title' => 'Tolak dan Hapus',
                        'detail' => ''
                    ]
                ]
            ],
        ],
        'trashed' => [
            'user' => [
                'user' => 'Tong Sampah User',
                'detail' => 'Lihat users yang sudah di hapus',
                'modal' => [
                    'restore' => [
                        'title' => 'Kembalikan User',
                        'detail' => '',
                    ],
                    'delete' => [
                        'title' => 'Peringatan !!!',
                        'detail' => '',
                    ]
                ]
            ],
        ],
        'unit_kerja' => [
            'unit_kerja' => 'Unit Kerja / Fakultas',
            'detail' => 'Mengatur Unit Kerja / Fakultas',
            'add' => 'Tambah Unit Kerja / Fakultas',
            'modal' => [
                'update' => 'Update Unit Kerja / Fakultas',
                'delete' => 'Delete Unit Kerja / Fakultas'
            ]
        ],
        'register' => [
            'register' => 'Register',
            'detail' => 'Buat User Baru',
        ]
    ],

    /**
     * Home - Bagian Profile
     */
    'profile' => [
        'profile' => 'User Profile',
        'detail' => '',
        'view' => [
            'view' => "Lihat Profile",
            'detail' => "Lihat Profile Kamu"
        ],
        'update' => [
            'update' => "Update",
            'detail' => "Update Profile Kamu"
        ],
        'password' => [
            'password' => "Password",
            'detail' => "Update Password Kamu"
        ]
    ]
];