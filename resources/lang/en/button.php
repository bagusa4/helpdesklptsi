<?php
/**
 * Created by PhpStorm.
 * User: Bagusa4 - Smecon
 * Date: 4/2/2017
 * Time: 7:07 PM
 */

return [
    /*
    |--------------------------------------------------------------------------
    | Button - button
    |--------------------------------------------------------------------------
    |
    | Nama dari setiap button
    |
    */

    'refresh' => 'Refresh/Segarkan',
    'print' => 'Cetak / PDF',
    'filter' => 'Filters',
    'reset' => [
        'reset' => 'Reset',
        'form' => 'Reset Form',
    ],
    'confirmation' => [
        'cancel' => 'Batal',
        'save' => 'Simpan',
        'delete' => 'Hapus',
        'update' => 'Ubah',
        'accept' => 'Terima',
        'decline' => 'Tolak',
    ],
    'tiket' => [
        'create' => 'Buat Laporan/Keluhan'
    ],
    'submit' => [
        'submit' => 'Submit',
        'tiket' => 'Kirim Laporan/Keluhan',
    ]
];